From: PEARLTAPE TDY TITAN <pearltape@sec.titan.org>
To: pearltape@file.mi.mil.luna.gov
To: forint@mil.luna.gov
To: research@extsec.titan.org
Delivered-To: pearltape@file.mi.mil.luna.gov
Received: from gateway14.mil.luna.gov
    by 3.file.mi.mil.luna.gov
    with ESMTPSA id kUQ992ci1Z3vr (enc ok sign ok)
    for pearltape.file.mi.mil.luna.gov
Received: from relay3.local.rs001.l4.earthsys.gov
    by gateway14.mil.luna.gov
Received: from qec.titan.org
    by relay3.qec9.rs001.l4.earthsys.gov
Subject: PEARLTAPE//TRANSCRIPT/OBINT/DEBR/0274/1
Date: 19 Sep 2421 00:38:19 +0000
Date-Local: 19 Sep 2421 00:38:19 +0000
X-Classification-Marking: TS/PEARLTAPE/EYES ONLY ALLIANCE
X-Catalogue-Serial: PEARLTAPE//TRANSCRIPT/OBINT/DEBR/0274/1
Content-Type: text/plain; charset="utf8"

--------------------
TOP SECRET PEARLTAPE
--------------------
HANDLE VIA QUILL CHANNELS ONLY

PEARLTAPE//TRANSCRIPT/OBINT/DEBR/0274/1

FULL CATALOGUING DATA FOLLOWS

TRANSCRIPT BEGINS

[Subject NINE is asleep or unconscious. Subject LEE sits in a chair
at her left side, apparently dozing.]

NINE: [INDISTINCT]

[Subject NINE coughs. Subject LEE startles slightly and sits up.]

LEE: Oh, chérie, welcome back! Here, sip this -

[Subject LEE holds a cup of water for subject NINE. Subject NINE
takes the straw and drinks, then coughs.]

LEE: Easy.

NINE: [INDISTINCT] better, thank you - Sameen? What are you doing
here?

[Subject NINE looks around.]

NINE: What am I doing here? Where are we? What's happened?

LEE: We're on Titan, and you're safe. What do you remember?

[Subject NINE delays answer, apparently considering the question.]

NINE: I was in my crash couch. We all were. They were going to
shoot at us any second, and I was so scared. I was sure every
second that - then I felt the ship turn hard, and then a second
later something hit us. My couch must have broken, I felt myself
spinning, and then -

[Subject NINE considers.]

NINE: Just flashes, and then I woke up here.

LEE: That was three days ago.

NINE: Three days? But why didn't they -

LEE: After the captain sent his last report to Luna Lines, their
military liaison messaged Ambassador Vasu here on Titan. She talked
to Ambassador Branislav, and then they went and talked to - oh,
it's a long story, but they figured out where your ship was, and
And Yet It Moves got to you just barely in time.

NINE: The Titan ship?

LEE: The same. They shot down Kearsarge's first missile before it
fused, and there was no second one. But the missile was so close
that some of the debris still hit you.

NINE: Oh.

LEE: Your couch didn't break. They say it saved your life. But -

NINE: I know that look. Sameen, just tell me.

LEE: Amelia, can you feel your foot? Your left foot. Can you move it?

NINE: Sure, I -

[Subject NINE looks down at the bed, observing her injury. (L leg
traumatic amputation distal to knee, full details in med report,
file ref PEARLTAPE/DEBR/79) Psychometric overlay indicates response
within error bars.]

NINE: Oh.

NINE: Oh shit.

[Subject NINE begins to cry. Subject LEE leans over the bed to hug
subject NINE.]

LEE: [INDISTINCT] going to be okay. You're [INDISTINCT].

NINE: [INDISTINCT]

LEE: I - let me call in the nurse, and -

NINE: No! I need to -

[Subject NINE shakes off subject LEE, wipes her eyes, and grasps
the bedsheet. Subject LEE places her hand atop subject NINE's.]

LEE: Lia, please. Let me help, at least.

[Subject LEE helps subject NINE into a sitting position against the
headboard, then pulls aside the sheet, exposing subject NINE's hip
and unbandaged left leg.]

NINE: God. Three days?

[Subject NINE sobs.]

LEE: They gave you healing adjuvants. That's why you've been out so
long.

NINE: It feels like it's still -

[Subject NINE leans forward, shaking aside her IV tube, and reaches
into the space below where her left leg ends.]

NINE: Oh, that's -

[Subject NINE chokes, then retches and heaves. Subject LEE holds an
emesis tray for subject NINE.]

NINE: Ugh.

LEE: It's okay. Here. Rinse out your mouth.

[Subject NINE does so, then looks at her leg again. Subject NINE
raises her left thigh, carefully touching the stump.]

LEE: Does it hurt?

NINE: No, not much. It's just -

LEE: Weird?

NINE: I keep thinking it's still there and I just can't see it.

[Subject NINE presses the stump with her fingers.]

NINE: I can still feel it. But I can feel this too. It's -

[Subject NINE swallows hard. Subject LEE reaches for a clean emesis
tray.]

NINE: No - no, I'm okay.

LEE: Okay.

NINE: Will you -

[Subject NINE takes subject LEE's hand and moves it toward her
stump. Subject LEE rests her hand on subject NINE's left thigh,
just above her knee.]

NINE: It's okay if you can't.

LEE: No, it's not.

NINE: No.

LEE: It's okay. I just -

[Subject LEE begins to cry, and moves her hand down to rest on
subject NINE's stump. Subject NINE places her hand atop subject
LEE's and presses it against her stump.]

NINE: Thank you -

LEE: [INDISTINCT] see you like this.

NINE: I'm sorry.

[Subject LEE coughs, then laughs.]

LEE: You're - you are a nonpareil, chérie.

[Subject LEE wipes her eyes with her free hand.]

LEE: You've just had your leg shot off and here you are,
apologizing to me.

[Subject NINE laughs.]

NINE: And why not, madame? Am I the only one inconvenienced?

LEE: I would hardly call it an inconvenience!

NINE: Well, it is more than that. It still doesn't seem real.

[Subject NINE begins to cry.]

NINE: But I was sure I would die.

LEE: Here, move over -

[Subject LEE steps out of her slippers and climbs with some care
into subject NINE's bed.]

NINE: Mind the -

LEE: [INDISTINCT]

NINE: [INDISTINCT]

[Subject NINE curls against subject LEE, sobbing. Subject LEE pulls
subject NINE close.]

LEE: You made it, Amelia. You're here. You're really here, and
you're safe now! You're safe.

[Subject NINE continues to cry while subject LEE comforts
her. Psychometric overlay indicates delayed fear response
consistent with subject NINE's experiences. Predictive function
indicates healthy adaptation contingent upon continued strong bond
and concomitant supoprt. Strongly recommend full debrief implement
A09(c)§3.146 variance per Y. Laporte.]

NINE: I still can't believe they let me go.

LEE: They never had you. Kearsarge -

NINE: No, I mean - back on Ganymede.

LEE: Do you want to talk about it?

NINE: No.

LEE: Okay.

NINE: They kept bringing me in for days, after they'd stopped
talking to everyone else. Asking questions about who I knew on
Luna, how I knew them - what we did, what we talked about. The same
questions over and over, and I didn't know how much they already
knew. After a while I wasn't even sure if I was lying.

LEE: Did you tell them about us?

NINE: Not at first.

[Begin audio clarity filtering and enhancement.]

NINE: I thought at first I could, you know. Leave it out.

NINE: But they kept asking the same questions over and over. Eight
hours a day, ten hours. I thought if I answered, told them what
they wanted to hear, they'd stop. But they didn't.

LEE: To find out if you were lying.

NINE: I guess. They weren't nice about it, either. Kept telling
me -

NINE: There was this one. He kept telling me how I'd abused my
position and Earth's trust. What a disgrace I am. That when they
were done with me, he'd send me to therapy, or Wyoming, and I
should be grateful for [INDISTINCT]

LEE: [INDISTINCT]

NINE: Yeah.

LEE: You don't have to put yourself through this right now.

NINE: No, I - I'm okay, I think. Just - it was bad.

LEE: I know. When did it start?

NINE: About two - no, three weeks ago.

LEE: And they let you chat with me?

NINE: He made me. Maintaining my pattern, he said. But only from
the interview room. They'd taken my hand unit away.

LEE: I'd wondered if something was wrong. You didn't sound quite
yourself.

NINE: I wasn't. [INDISTINCT] worst part?

LEE: Tell me.

NINE: They brought Joseph in.

LEE: No.

[End audio enhancement.]

NINE: They did! Acting so disappointed in that fake way he has. How
could I break his heart this way, how could I consort with another
woman and a lunar at that, after all he'd done to try to help me
cope with my problem. As if it hadn't been an arrangement all
along. But he was always better at playing the game.

LEE: [INDISTINCT]

NINE: All of it. They told me to - say what I said, at the end. His
idea, actually. Said it'd lend [INDISTINCT]

LEE: [INDISTINCT]

NINE: I told them [INDISTINCT]

[Subject NINE sobs. Subject LEE comforts her.]

NINE: I'm -

LEE: No, don't apologize. You had to. I don't mind. I'm glad it
wasn't worse.

NINE: [INDISTINCT] enough.

LEE: And about coming early? They told you to say that?

NINE: Yes. I thought they were being especially cruel. But then the
next day they just - let me go.

LEE: Let you go?

NINE: They did! Brought me in Thursday morning, sat me down just
like always, and then the same one from before came in and told me
they were sorry for the inconvenience. Thanked me for assisting in
their investigations and told me I was free to go. Even gave me
back my hand unit, wiped of course.

LEE: And that was it? Nothing about -

NINE: About being a disgrace? Therapy or Wyoming? Nothing. Just
that, and then an officer escorted me out.

LEE: What did you do then?

NINE: I went to work and tried to pretend nothing had ever been
wrong! But they'd cleared my calendar for me. All of it, not just
the appointments from while I was -

LEE: Being interrogated.

NINE: Yes. It's a frightening word. I suppose I know why, now. I
didn't stay long. No one wanted anything to do with me anyway. Went
home hoping Joseph wouldn't be there, and he wasn't. I packed a bag
and went to a hotel to - to wait it out until I could leave to meet
the ship.

[Subject NINE presses her cheek against subject LEE's
chest. Subject LEE comforts her. Begin audio enhancement.]

NINE: [INDISTINCT] so scared! That they wouldn't let me through to
the Luna side, at the checkpoint. They'd stop me, turn me back,
keep me there. I was crying when I finally got there, shaking so
hard I could barely stay on my feet, I was sure they'd stop me, but
they didn't. Just scanned my ID and boarding pass and waved me
through.

[Subject NINE laughs. End audio enhancement.]

NINE: And then I got my leg shot off.

[Subject NINE shifts against subject LEE, sitting up somewhat
against the headboard, and reaches to touch the stump of her left
leg.]

NINE: I could almost forget about that part, except that I
can't. But it's still a surprise every time I see it. Every time I
think about it.

[Subject NINE shakes her head.]

NINE: Why do you think they'd do that? Just let me go like that,
after everything.

[Psychometric overlay for subject LEE trends outside error bars in
several metrics, pattern suggesting apprehension and/or guilt.]

LEE: They weren't getting anywhere. They'd taken you through the
same questions enough times to know you weren't hiding anything
else, that they'd have seen it if you were. So they let you run and
watched where you went.

[Subject NINE looks carefully at subject LEE.]

NINE: A very specific answer.

[Psychometric overlay for subject LEE spikes outside error bars in
several metrics.]

LEE: It's not quite what our own counterintelligence people would
have done. But it's close enough. I haven't told you everything,
Amelia.

NINE: I've never asked you to.

LEE: No, and I've loved you for that, too. But you deserve to know.

NINE: All right, then. Tell me.

LEE: I told you I was a reserve major of infantry. I am a major,
but in active service with military intelligence. I'm a - a case
officer. A spy. And I've made you one as well.

[Security overlay flags unauthorized disclosure exceeding
A09(c)§3.146 variance; recommends intervention, recommendation
declined per Y. Laporte. Psychometric overlay for subject NINE
trends below error bars, pattern unclear.]

NINE: When did it start?

LEE: About a year after we met.

NINE: After.

LEE: Yes.

NINE: You didn't choose me from a list of lonely Earther women
known to have illicit tastes. They didn't give you my picture and
order you to go to that conference to seduce me.

LEE: I didn't -

LEE: I was still a reservist then, as I'd said. They reactivated my
commission about a month before Syria Planum.

NINE: You were a perfect monster at Syria Planum.

LEE: What was I to do? I couldn't leave you, Amelia. Not without
violating my orders and going to the stockade for it. They were
terribly serious about that. But I couldn't -

NINE: You tried to make me leave you!

LEE: They couldn't have done much about that. Just put me back on
half pay. I still had some friends in the MI establishment, people
who'd served with me. They'd have protected me. And I didn't -

NINE: You didn't want to feel like you were using me.

LEE: No.

NINE: You must have been furious when I came back.

LEE: I, furious?

[Subject LEE laughs.]

LEE: I'd never even seen you angry before! I wasn't sure you were
capable of it.

[Subject NINE laughs.]

NINE: You learned.

LEE: Yes! You told me exactly what you thought of the way I'd been
treating you. And you were right! You said that if I was done with
you, you'd thank me to show a little courage and tell me so, and
perhaps we'd salvage something as friends of what you'd imagined
would be our first time really together as lovers. And you said -

NINE: I remember. And then you started crying.

[Subject LEE begins to cry.]

LEE: You'd taken all my choices away from me.

NINE: You could have done worse.

LEE: I couldn't! I could hardly stand what I'd already -

NINE: I could see it hurt you, too. I didn't understand why. Why
was it so hard then?

[Subject NINE touches subject LEE's cheek.]

NINE: And why are you crying over it now?

[Subject LEE sobs.]

LEE: Because I love you!

[Subject NINE attempts to kiss subject LEE. Subject LEE pulls
back. Subject NINE catches the back of subject LEE's neck and
kisses her.]

NINE: It must have been so hard for you. All these years.

LEE: Hard for -

[Subject LEE wipes her eyes.]

LEE: Why are you taking this so well, chérie?

NINE: Perhaps I'm truly furious on the inside, madame.

[Subject LEE looks closely at subject NINE.]

LEE: No, I don't think so. When did you start to suspect?

NINE: Not long after Voortrekker left. You'd had so many questions
for us both - more than simple interest in your lovers' work could
explain. Especially when you asked me for that set of engineering
drawings.

LEE: Damn them, I knew I was taking a chance with that! Nothing
would do but that -

NINE: Oh, I believed what you were saying at the time, about the
hydroponics. It was only later, when Kit was gone and I was back
home, I started really thinking about it, and -

[Subject NINE laughs.]

NINE: For a spy, you are a very uneven liar.

[Subject LEE laughs.]

LEE: Hard to do one's best work when one's heart isn't truly in it.

[Subject NINE kisses subject LEE.]

NINE: You've shown me where your heart is, madame. True, I doubted
you for a while. But you reassured me.

[Subject LEE kisses subject NINE.]

LEE: I don't deserve you, chérie.

[Subject NINE laughs.]

NINE: No indeed! But perhaps, with enough effort, you may earn the
privilege.

[Subject LEE laughs.]

LEE: And all this time, you've never -

NINE: How would you have liked me to put it? And what if I'd been
wrong after all? How foolish I would have been!

[Subject LEE laughs and hugs subject NINE tightly. Subject NINE
reciprocates.]

LEE: I was sure you would hate me.

NINE: Oh, I might have, madame, I might have. To learn after almost
thirty years that I've been a spy, and you my spymistress? If you'd
told me a year ago, or a week ago -

LEE: What changed?

[Subject NINE disengages slightly from subject LEE, making room to
interpose her left leg between them.]

NINE: My perspective.

[Subject LEE places her hand on subject NINE's stump.]

NINE: Be gentle.

LEE: Does it hurt?

NINE: A little. It's okay.

[Subject NINE moves closer to subject LEE.]

NINE: Had you told me last week, I would have been heartbroken. I'd
have seen only that you cultivated my trust and then abused it. I
wouldn't have forgiven you. I wouldn't even have wanted to.

LEE: And now?

[Subject LEE looks away from subject NINE. Subject NINE places her
hand on subject LEE's.]

NINE: Tell me, madame. In what way have you betrayed me?

LEE: I -

NINE: You what? You've loved me? You've offered me solace I could
find nowhere else?

[Subject NINE begins to cry.]

NINE: You've helped me remember that I'm not wrong, I'm not
twisted, I'm not disgusting. That everything they tried to tell me
on Ganymede I am, that I've been hearing my entire life people like
us must be, I am not. You've brought me to meet your family, and
let them welcome me, make me feel as if I were not an aberration!
Is that how you've betrayed me?

[Subject LEE begins to cry.]

NINE: Or perhaps it was the times when you've taken me into your
home, into your bed! When you've helped me forget the life my
miserable nation forces me to lead, and - and the things I've had
to do, to try to hold on to what illusion of safety I could. When
you've made those things go away, for just a little while, and let
me imagine a life where I never had to face them again.

[Subject NINE kisses subject LEE.]

NINE: You foolish woman, why do you think I love you? So many times
you've been the only thing in the universe that reminded me of the
possibility of hope. Truly you are a monster, Sameen.

[Subject LEE laughs.]

NINE: And now here we are together. Whatever happens next, we'll
figure it out together. I'd never dare hope for so much.

LEE: But your leg -

NINE: I'd have given up more.

[Subject LEE kisses subject NINE. Subjects hug one another
tightly.]

NINE: [INDISTINCT]

LEE: Now, chérie?

NINE: [INDISTINCT]

LEE: [INDISTINCT]

TRANSCRIPT INTERRUPTED

APPX 40 MIN EXCLUDED PER Y. LAPORTE

OVERLAYS DISABLED

NO RELEVANT CONTENT EXCLUDED

TRANSCRIPT RESUMES

NINE: You're right - Lunar hospitals really are better than
Earth's!

[Subject LEE laughs.]

LEE: I've never known them to be quite that good.

NINE: Well, I'm sure it will speed my recovery.

LEE: All to the good. You need a bath, chérie.

[Subject NINE laughs and swats subject LEE.]

NINE: Monster! And you don't?

LEE: I am a rose, fresh with morning dew.

[Subject NINE laughs.]

LEE: Come, slugabed! Let's get you to your foot.

NINE: You are the very picture of sympathy.

[Subject LEE stands and extends her hands to subject NINE, who
takes them and pulls herself up.]

LEE: Easy - you've been on your back for three days, don't -

NINE: Oh, it's nothing! The gravity here is so light, it's as if -

[Subject NINE sways. Subject LEE catches her.]

NINE: Damn! I forget, and try to -

LEE: Here, lean on me.

[Subject LEE wraps her arm around subject NINE's back; subject NINE
reciprocates.]

NINE: So tall.

LEE: Okay now?

NINE: I think so. Let's - let's stay here a moment, and see.

LEE: Of course.

[Subject LEE kisses the top of subject NINE's head.]

LEE: It's strange, you know.

NINE: My hair?

LEE: True, we have few redheads in Luna - one more soon, to my
delight. And so prettily freckled!

NINE: Spare my blushes, please, madame.

LEE: And deny myself such a sight? But no, I mean that they let you
go and then came after you. It doesn't make sense.

NINE: Oh! Here, I'll show you.

[Subject NINE looks around.]

NINE: Where are my things?

LEE: Your clothes were ruined, I believe. But -

[Subject LEE leans, careful not to upset subject NINE, and
retrieves a bag of subject NINE's effects from a drawer in the
bedside table.]

NINE: Ah - is my hand unit there?

LEE: Yes, there at the bottom. Here -

[Subjects LEE and NINE, working together, open the bag, and subject
NINE retrieves her hand unit. NB: Device inspected per A09(c)§1.38,
software implants identified and removed, transceivers disabled per
file ref PEARLTAPE/DEBR/81a. Data dump file refs
PEARLTAPE/DEBR/81b1 (primary store) PEARLTAPE/DEBR/81b2 and
PEARLTAPE/DEBR/81b3 (substores encrypt/stegano per A09(b)§4.10).]

NINE: Still charged.

[Subject NINE unlocks the device, then enters a long passcode and
reauthenticates via optical and touch sensors.]

NINE: I set this up last year, the same way you showed me for our
messages and logs. I thought they must have found it and wiped it
too, but when I checked in the shuttle, it was still there. Look -

[Subject NINE hands the device to subject LEE.]

LEE: Calisse de tabernac!

[Subjects NINE laughs. Subject LEE kisses subject NINE.]

LEE: Is this everything?

NINE: Everything I had.

LEE: Maudit! Engineering drawings, documents, correspondence - and
they must not have realized until after you were away. No wonder
they came after you! Bad enough you should tell our attaché aboard
the shuttle, but to have you bring us this -

NINE: Do you need to, to call someone, or take that somewhere?

LEE: No, they'll have dumped it before they released it to the
hospital. My love, you'll have a medal for this!

NINE: I don't need a medal.

[Subject LEE kisses the top of subject NINE's head.]

LEE: No, but you deserve one. More than anyone with whom I've ever
served.

[Subject NINE laughs.]

NINE: My blushes -

LEE: Are lovely as always. But I'm quite serious. I am a soldier,
and we must be brave, to be what we are. We're trained to withstand
fear, to do what we must in spite of it. You, though -

NINE: Sameen, you don't -

LEE: Amelia, listen, I know how you've doubted your courage. But
what you've - everything you've done to be here. I would not have
accepted such a mission if it had been given to me! None of us
would. We'd have protested to command, gone to the stockade, rather
than take on such a completely impossible series of tasks as the
ones you set yourself.

[Subject LEE pulls subject NINE into a tight hug against her chest,
careful not to disturb subject NINE's balance.]

LEE: I have always suspected you to be a madwoman, chérie, so I am
not surprised. But mad or not, you are the bravest person I've ever
known.

NINE: [INDISTINCT]

[Subject LEE leans down and kisses subject NINE firmly.]

LEE: Don't try to argue! I will not call my lover a liar. You'll
only force me to -

[Subject NINE yelps, then laughs.]

NINE: You tease. You monster!

[Subject LEE laughs.]

LEE: A monster, perhaps. But a tease?

NINE: You are a monster. Lucky for you, though, [INDISTINCT]

LEE: Goodness. Well then, ma petit chérie puante, let's go and find
that bathtub, shall we?

NINE: You'll have to let me go a little, first -

LEE: I will not!

[Subject LEE lifts subject NINE into her arms. Subject NINE
laughs.]

NINE: Oh, you make me feel tiny!

LEE: Shall I put you down, then?

[Subject NINE wraps her arms around subject LEE's neck and pulls
subject LEE's head down for a kiss.]

NINE: Never.

[Subjects LEE and NINE exit observation area via en-suite door. At
this time Y. Laporte terminates observation and dismisses OBINT
team. No further interaction observed.]

TRANSCRIPT ENDS

--------------------
TOP SECRET PEARLTAPE
--------------------
HANDLE VIA QUILL CHANNELS ONLY

CATALOGUE CODEWORD:
     PEARLTAPE

CATALOGUE INDICATORS:
     TRANSCRIPT/OBINT/DEBR/0274/1

DATE:
     2421-10-18/20:11:14/+0000

DURATION:
     02:31:17/TWO HOURS THIRTY ONE MINUTES SEVENTEEN SECONDS
     EXCLUSIONS TOTAL 41:19/FORTY ONE MINUTES NINETEEN SECONDS

LOCATION:
     ROOM 417/YANG MEMORIAL HOSPITAL
     ARRAKEEN TOWER/ARRAKIS PLANITIA
	 TITAN

SUBJECTS:
     SAMEEN LEE/MAJ/MI/IO3 J41029 PEARLTAPE CASE OFFICER
     AMELIA NINE/EARTHGOV EXPN SPT DIR VOORTREKKER (FMR)
     ..FKA/AMELIA YOUNGER

OIC:
     Y. LAPORTE/LTC/MI/SIO J33545 PEARLTAPE ACTUAL

TRANSCRIBER:
     M. UNDINE/2LT/MI/IO1 J65849 OBINT OFFICE

NOTES:
       THIS TRANSCRIPT DESCRIBES ROUTINE OBSERVATION OF UNDECLARED
     ..DEBRIEFING SESSION PER A09(B)§4.12
       OBSERVATION ENDS PRIOR TO END OF INTERACTION PER Y. LAPORTE
     ..REF A09(C)§3.181
       RECORDINGS AND OVERLAY DATA PRESERVED PER A09(B)§4.3. ACCESS
     ..ONLY PSYCH ANALYSIS STAFF WITH TS/PEARLTAPE/INTSEN CLEARANCE
