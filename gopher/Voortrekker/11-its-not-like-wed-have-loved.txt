From: QEChat Log anine <-> sam <noreply@qechat.earthsys.gov>
To: anine (Amelia Nine) <anine@expeditionsupport.gov>
To: sam (Sameen Lee) <sameen.lee@recoveryinstitute.org>
Delivered-To: Sameen Lee <sameen.lee@recoveryinstitute.org>
Received: from relay3.qec7.rs001.l4.earthsys.gov
    by mta1.recoveryinstitute.org
    with ESMTPS id CACAEDA29A5ECB
    for <sameen.lee@recoveryinstitute.org>
Received: from relay2.qec5.ganymede.earthsys.gov
    by relay3.qec7.rs001.l4.earthsys.gov
Received: from host3.qechat.earthsys.gov
    by relay2.qec5.ganymede.earthsys.gov
Date-Local: 08 Sep 2421 19:22:07 +0000
Date: 08 Sep 2421 19:22:07 +0000
Content-Type: text/plain; charset="utf8"
Subject: Chat Log anine <-> sam

[18:39]
anine: hey
sam: hey!
sam: how's it going out there? haven't heard from you
anine: been super busy! finally settled down a little
anine: we heard back from gliese, not as bad as we thought
anine: all still concerned over ross but watch & wait seems to be
 the plan
anine: anyway, not what we're here for, i'm glad you heard from
 kit!

[18:02]
sam: me too!
anine: and i'm glad you answered her. if you want to know what i
 think, you waited longer than you should've to do that.
sam: you're right.
sam: i hope you don't mind i sent your love too
anine: no! Not at all
anine: i would've asked you to already but i wasn't sure if

[18:03]
anine: i mean it's been so long, i wasn't sure
sam: come on, you know kit, she can't have changed THAT much
anine: yeah but i always thought, you know
anine: eh, forget it

[18:04]
sam: what
anine: no it's nothing
sam: no it's not.
sam: talk to me

[18:07]
anine: do you know, we talked about marrying?
anine: kit and me
anine: it would've solved the biggest problems for both of us
anine: wouldve been perfect

[18:09]
sam: your idea or hers?

[18:10]
anine: hers
anine: you know i'd never have suggested it
sam: me either
anine: and she really tried
anine: she tried sO HARD

[18:11]
anine: but she coudlnt
anine: couldnt stand it, i made her stop
anine: i could see it was killing her but she wouldnt stop until
 she had a panic attack
anine: even aftert hat she wanted to try again

[18:12]
anine: i wouldnt let her
anine: we both felt terrible about it
anine: but then i went and got married anyway
anine: because
anine: it was easier that way, i could deal better than she could
 and it did solve a lot of problems for me

[18:13]
sam: the same ones she had
anine: yes
anine: and wouldnt so lve

[18:14]
anine: I mean if she'd just go to therapy
anine: it's not like we'd have loved her any less
sam: no
sam: but she wouldnt have wanted to be with us after that
sam: and you know wjy she wouldnt do it anyway

[18:15]
anine: yes
anine: she wouldn't compromise the way i ddi
anine: did*
anine: she held on to herself in a way i couldnt
anine: and god knows i HATED her for that

[18:16]
anine: for a long time
anine: i mean she had it so much worse than me and she just
anine: WOULDNT
anine: i mean
anine: she made me feel like

[18:17]
sam: you felt like a coward

[18:18]
anine: yeah
anine: that she could deal and i couldnt

[18:19]
anine: and then she joined voortrekker and that was even worse
anine: here she was, ready to go to another STAR STYSTEM
anine: because maybe there it'd be better
anine: and i mean

[18:20]
anine: we had such a hard time selling the no kids requirement
anine: i think that was as much as anything what decided her
anine: and, i don't know

[18:21]
anine: i just never felt like i could face her after all that

[18:22]
sam: so that's why you didn't come see us before she left
anine: between that and the fight
anine: yeah

[18:24]
sam: look, i'm not supposed to tell you this, okay
sam: but you need to hear it

[18:25]
sam: the last night kit was here before she went to join the ship
sam: we stayed up all night talking
sam: and that came up
anine: oh shit
anine: oh no

[18:26]
sam: NO
sam: stop
sam: listen
sam: you know what she said?
sam: she said she wished she was as brave as you

[18:27]
anine: what
sam: YES
sam: she didn't see it as you making a cowardly compromise
sam: i know that's how YOU see it
sam: but that's wrong
sam: and kit KNEW that was wrong
sam: you did what you had to do

[18:28]
sam: to have the best life you could with this bullshit government
 youre stucck with
sam: kit respected that
sam: and she wished you'd decided to come
sam: so she could tell you she was sorry she couldn't help you

[18:29]
anine: on bulslhiy
anine: l
anine: loko i appreciate you tryign
anine: to make me feel better but
anine: come on we both know thats bullshit

[18:30]
sam: Dry your eyes and think a minute.
sam: You know me better than that.

[18:32]
anine: Yeah.

[18:33]
anine: I'm sorry.
sam: nothing to apologize for

[18:34]
sam: im sorry i didn't tell you before.
sam: if i'd known you felt this way about it
sam: but i shouldve told you.
sam: i'm sorry.

[18:35]
anine: its ok
anine: i need a minute
anine: brb

[18:49]
anine: you here?
sam: still here.
anine: look ive got a bunch of leave saved up
sam: you've got some leave right?
sam: haha
anine: YES haha

[18:50]
anine: so like i said things have settled down around here
anine: why dont i come see you before next friday
sam: can you get away?
anine: yea theres a luna shuttle leaving night after next
anine: ive been "mentioning" im going to stop on luna after i
 rotate out

[18:51]
anine: to see what i can find out unofficially about the gliese
 mission
sam: makes sense, ill tap my contacts over at the extrasolar office
sam: maybe get you something to take back, save you some time
sam: at least let them know youre coming

[18:52]
anine: ok
sam: its a good excuse
anine: haha yea
anine: even with joseph
anine: hes mad anyway with all the work lately

[18:53]
sam: not going to cause more problems?
anine: than i already have? maybe
anine: but you'll make me feel better about them
anine: wont you
sam: you know i'll do my best

[18:54]
anine: your best is mighty good, lover
sam: pas devant le CQE, ma cherie!
anine: haha ok ok

[18:55]
sam: save it for when you're here.
anine: oui, madame!

[18:56]
sam: i wish i didn't
sam: but i do still have some work left today
anine: i'll leave you to it, then.
anine: you'll tell me if you hear more from kit?
sam: of course
sam: i can tell her you want to hear from her

[18:58]
anine: not yet i think
sam: okay
sam: let me know when you're ready

[18:59]
anine: thank you!
sam: de rien.
anine: and

[19:00]
anine: send her my love?
sam: of course.

[19:01]
anine: off to go break the news
anine: and be properly apologetic.
sam: good luck!
anine: i'll be thinking of you!

[anine: Quit: it really does help..]

[sam: Quit: ]
