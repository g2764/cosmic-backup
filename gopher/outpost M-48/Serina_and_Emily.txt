###INCOMING TRANSMISSION###
source: outpost M-48
author: Jack "Sarge" Carter
subject: Serina and Emily

---BEGIN MESSAGE---
I've been getting along pretty well past the last week. First I met
a caravan that wandered around my area - I've set up a camp in one
of the ruins of a building, the place had some furniture intact.
The caravan traded some furs of animals I've hunted for some
clothes and even gave me a dog to keep me a company. It's a female
husky that I named Serina - a name after very old and very dear
friend of mine - a friend that I lost a long time ago. There's
something about dogs and their unconditional love - her brown puppy
eyes have that special sparkle in them and her tail wagging happily
every time our gazes meet, as if she understood every word I tell
her. Either way it's good to have someone to watch my back - and
that might be more necessary now than ever. Last night I heard
strange noises coming the outside of my shack. It was chittering,
clicking, and scratching - like some kind of bug or something. The
next morning I went out with my bow ready, but everything seemed
just fine - except for some of my food supplies missing. Perhaps
some kind of animal perhaps - although a very strange one, because
the tracks it left were nothing like I ever seen. The trail led us
to a cave entrance not too far from our base. Serina seemed nervous
as we got close. I thought it'd be probably best if I secured my
stash a bit better next time.

A couple of days later I've heard another thief paying a visit to
my stash - this time though, the sound they made was different.
Imagine my suprise when I kicked the door in wielding a huge club
in my hand ready to strike, only to realize that I was dealing with
a feral looking woman. The moment she saw me, she let out a scream,
and like a cornered animal, she launched herself at me trying to
punch her way out of the desperate situation she ended up getting
into. Needless to say she underestimated her opponent and ended up
on the floor knocked out cold. Before she came to her senses, I
locked her up in makeshift prison cell I've made out of one of the
ruined buildings near my shack. I tried to talk to her to find out
what the hell was she thinking stealing from a stranger, but to my
suprise, her only means of communications were whines, growls
and...noises. She didn't seem to understand what I was saying. And
by the looks of things, she's been brought up among wolves -
literally. I tried to communicate through gestures, mostly I wanted
her to know that I mean no harm to her, and did that little
pointing game from those old movies when people with a language
barrier try to introduce themselves. As a result, at least I've got
the girl's name - Emily...
---MESSAGE END---
