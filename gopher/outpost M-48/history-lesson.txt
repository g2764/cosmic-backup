###INCOMING TRANSMISSION###
SOURCE: Outpost M-48
DECRTIPTION:Transcript of a history lesson conducted by prof.
Judith Carter on [ERROR: DATA CORRUPTED]

---BEGIN TRANSCRIPT---
Good morning everyone, My name is professor Judith Carter, and
today we'll be looking at origin of the infamous non-involvement
law. As you may know, our federation known as Earth Union has been
expanding for about five centuries now. As of now, there are
officially 85 member planets in this federation and counting. This
expansion has been greatly aided by discovery of faster-than-light
methods of travel including time-compression, sub-space jumps, or
warp travel. The method is usually same, but different pop-cultures
tend to have different names for it.  Anyways. As some of you know,
some of these planets actually had native inhabitants when we
colonized them. This has led to problems, because in some cases,
these civilizations haven't been developed enough to establish
first contact, but we did anyways - as if the humanity didn't learn
from it's past mistakes. As a result, these primitives had become
increasingly reliant on the advanced technology we have introduced
to them without being even able to fully understand it, which has
lead to mass extinction.  Don't get me started on how some
companies just went in and sucked some planets dry of their
resources... Some of you may have heard of Terrania XXII where we
had to migrate the native civilization into natural reservations -
similar to what we did to native americans back in the day. To
prevent any further damage, our government has introduced so called
"Law of non-involvement".  Basically, when one of our ships
discovers a new habitable planet, they have instructions to record
it's coordinates and deploy scanner satellites and assign category
based on presence of life-forms civilizations, and it's development
stage. There are five categories from A to F where F is for barren
planet with no intelligent life or exploitable natural resources -
good example is Venusia which has been bought by private company
that has transformed this planet into enormous casino and luxury
brothel - all the way to category A, where civilization is advanced
enough for us to establish first contact and engage in diplomacy -
good example is our recent addition, the Denebian Empire with their
home planet orbitting around Alpha Cygni in Cygnus constellation,
also known as swan.  Unfortunately, trying to determine the rate of
civilization's advance based on satelite surveilance has proven
inconclusive and since simply sending the probes is not an option,
an agency for primitive planet oversight has been established.
Unfortunatelly we're running out of time, so we're gonna have to
save that topic for another lesson. Class dismissed.
---END TRANSCRIPT---
