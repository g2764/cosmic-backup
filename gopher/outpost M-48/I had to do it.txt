###INCOMING MESSAGE###
Source: Outpost M-28
ID: Jack Carter

---BEGIN MESSAGE---
By now there should be a wolfhound class capital ship orbitting
around Mars, with a corporate laptog snooping around looking for
answers. They won't find any. All they'll get is a hole in a
ground, with some ruins still smoking. That place could not be
tolerated. The outpost M-28 should never have existed. At least not
with it's true purpose.
You probably follow the news. Like so many others who are trying to
find comfort in their miserable lives knowing that someone else has
it much worse than they do. It makes me sick. What they don't tell
you, are those thousands upon thousands of people going missing.
The media are controlled by politicians, who employ them to further
their own agenda. They don't want you to know. They don't care.
Entire colonies go missing at moments notice. Nobody notices. And
those who do are quickly silenced. They don't want you to know
what's really going on. They only want to make a profit. They only
want to cash in on their own invention. You know what I am talking
about. Computer devices so powerful, capable performing the
calculations so complex, that the interstellar travel has become
trivial as a result. The Interstellaria devices. Bunch of
entrepreneurs have made fortune during that new gold rush that
these devices suddenly enabled. But at what price? Have you ever
considered? Have you ever wondered? All those missing people. They
are the very thing that powers those computers. Don't believe me?
It's okay. I found it hard to believe myself. A computer made of
people. How riddiculous right? A biological construct powering
synthetic machine. I mean, cyborgs and remotely-controlled drones
are common enough. But this technology was different. Each
individual cell broken down to it's basic components and then
rearranged to create something completely new. A synthesis. They
claim that this is a next step in humanity's evolution. That we are
this close to becoming gods. I fear that this might end up
destroying us instead. We are not ready for this. Not yet.
To the captain commanding the capital ship. There is nothing here.
Go home. Your mission is accomplished here. There's nothing more
you can do. Turn away now. Warn your superiors, because we are
coming to make things right. We will...
---END MESSAGE---
