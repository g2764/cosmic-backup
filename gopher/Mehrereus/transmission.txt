\[E;=35\[E;]=22
\[N;"LOGIN ******";
\[N;"--AUTHORIZE--";
====
THIS IS AN AUTOMATED MESSAGE FROM 0x4e657264
====
\[F;"--HEADER--";
QEC-DIMENSION=3954
TERMINAL=xvt-3980
SHIP=MH-8494e6
\[F;"--ENTRY--";

Unauthorized access has been detected.
Enhanced security measures enabled.

===

If you're reading this, that means you've found our ship
Leave it now. Please, for your safety.
You cannot fight them.
Although if you're reading this, it might be too late.

===
2/21/86/17/100
Captain Brets
629	418	470	965	978
177	165	980	208	506
635	977	431	151	175
930	840	859	347	494
892	661	567	182	279
655	449	574	114	928
===
\[N;"--EOF--";
