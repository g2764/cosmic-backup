E. Grosvenor log entry t-1
The Space Beagle is departing in the morning.
Although the rest of the crew seems competent I am the only 
Nexialist aboard.
I am getting used to sideways looks.
They don't seem to know what to think of me.
They are mostly old space hands.
But none of us has done anything like this before.
If all goes well we may return before we die.
The more I explain the odds against everything going well
the less they want to talk to me.
I did the math, I know I'm right.
That doesn't make me popular.
The next time I make a log entry we'll all be in space.
I'm as excited as a podcaster.
