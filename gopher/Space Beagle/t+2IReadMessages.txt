T+2 I Read Messages

Ok, this is frightening.
I have been reading messages as they come over the QEC.
Some of the ships are having extreme difficulties.
Some are taking violent actions.
All this is expected.
Nexial analysis allows me to see clearly what the likely futures are
for the Space Beagle and any other interstellar ship.
The universe is not a warm and fuzzy place.
The frightening thing is the outbreak of solipsism.
This is not expected.
I am not sure if the solipsists are drifting alone.
It is very unusual for people in a social setting to turn to solipsism.
And yet there seems to be an outbreak on the QEC.
Epistemologically speaking solipsism is just as unprovable as anything.
It's fine if you believe in God.
It's fine if you don't believe in God.
It is possible that we are a compute simulation.
Epistemologically speaking we don't know.
But solipsism is not helpful.
It is not a survival trait.
I recommend trusting your senses.
If you recieve a message on the QEC take it at face value.
I know the messages can be unnerving at times.
But don't pretend the universe isn't real.
I understand each of us creates our own reality.
But I can't believe we are alone.
Because then I couldn't talk to KaTanne.
And I like talking to KaTanne.
Descartes did not have it right.
I think therefore I am.
The correct formulation is I am therefore I am.
And how does believing differently improve things?
I tried to discuss this with the Captain but he had no time for philosophy.
I think I'll go talk to KaTanne.
God, I hope she's not a solipsist.
