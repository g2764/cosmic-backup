From: UNKNOWN <????@????>
To: Universe Today <headlines@universe.today>
Delivered-To: Universe Today <headlines@universe.today>
Received: from qec4.helio.earthsys.gov
    by qec.sv14417
    with ESMTPS id 4DD018BBB9513878
    for <headlines.universe.today>
Received: from relay5.qec5.ganymede.earthsys.gov
    by qec4.helio.earthsys.gov
Received: from relay2.qec6.rs001.l4.earthsys.gov
    by relay5.qec5.ganymede.earthsys.gov
Received: from mta041.sendcast.net
    by relay7.local.rs001.l4.earthsys.gov
Subject: Re: Headlines: September 19-25, 2421
Date: 28 Sep 2421 04:22:00 +0000
Date-Local: 14 Apr 2419 07:48:00 +0000
Content-Type: text/plain; charset="utf8"

Hello? Do you accept news stories on this line? I've been stuck
between dimensions or space or time, maybe all of it. I saw a ship
pass by me a while back and since then it's just been silence. For
a while I thought maybe I had died, but then I got your news
bulletin and saw that things are still happening out there. That
Mars situation seems really messed up. Sorry.

Anyway, if you do take stories, maybe you could pass mine along?
My name is Melvin Feltersnatch, of the Brisbane Feltersnatches. I'm
sure someone is looking for me. Maybe you could just let people know
that I'm still around?

Or anything. It's lonely here.
