----
From: Malkonkordo Research Vessel
Destination: SOL
Position: 842.78, -547.57, -3278.63
Departure: 7941.037.17
Shipdate: 0000272X
Mode: Search
----

# Status Update

* Heading to Enketu Tri

# Status Report

Captain's Log SD272X

To follow-up on the incident of SD2726, the two detained engineer's innocence 
has been confirmed through the archival footage.
However, what disturbs me is that it appears that the damage was due to impact 
from a teapot hurtling through space that collided into the ship at tremendous 
speeds into the section of the ship holding our communication systems.
The likelihood of these events seem so highly improbable, some may even see 
this as a message from their god/goddess or proof of a higher power for any 
non-believers, but we are not so foolish and know that the cosmos is a cruel 
mistress that fears not to through misfortune of any probability to the 
unexpecting.

With that being said, our primary communication systems are still down and it 
has now been 4 ship days without contact to Dekaoso Prime.
Every moment that passes it becomes more imperative that we resolve this matter 
and restore connection with Dekaoso Prime.

Fortunately, this may come to pass as we are now making a change in course 
towards Enketu Tri to hopefully rendezvous with another ship for assistance 
repairing our communication systems.
For those without access to the Dekaoso star maps, we first pity you for your 
lack of our superior knowledge of the universe around us. 
Secondly, from our observation equipment it shows that Enketu Tri appears to 
have a large red storm on it's surface that would be clearly visible from 
orbit, and the adjacent planet nearest this system's star has a red surface 
that our sensors indicate is likely from oxidized iron.
If before any could not before meet up with Malkonkordo to assist us at Enketu 
Tri due to lack of proper information, this should hopefully be enough to 
determine it's location on your inferior star maps.

The Malkonkordo is still expected to arrive on SD2735 and will remain in orbit 
until SD2739 to gather data for our research expedition and await any ships 
seeking to assist us.
After which time, on SD2740 we will be departing from Enketu Tri to begin our 
search of the "SOL" system.

Praise be to the Sinjorino and Alportas Majeston.

~ Captain Kiu Serĉas

----
HTBlBUqv2tiiTbAYi/2dKHwPEDYKT9VPXJAg1E6Op7g=
