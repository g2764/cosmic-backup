```
        ___________.    __    ________              ________ .__   
__  _  _\_____  \_ |__ |  | __\_____  \______  ____ \_____  \|  |  
\ \/ \/ / _(__  <| __ \|  |/ /  _(__  <_  __ \/    \  _(__  <|  |  
 \     / /       \ \_\ \    <  /       \  | \/   |  \/       \  |__
  \/\_/ /______  /___  /__|_ \/______  /__|  |___|  /______  /____/
               \/    \/     \/       \/           \/       \/      
                   .___.__                                         
____________     __| _/|__| ____                                   
\_  __ \__  \   / __ | |  |/  _ \                                  
 |  | \// __ \_/ /_/ | |  (  <_> )                                 
 |__|  (____  /\____ | |__|\____/                                  
            \/      \/                                             
```
W3bK3rN3l radio outpost
wbknl-000019
-----------------------
KG-84 encrypted MIL-STD 188-110B
Message made on an ISB circuit
--> Croughton (AJE)
--> Sigonella (NSY)  

{
Several theorists suggest that this is a secret communication
station for a secret society, agency, or government, to be used
to communicate in an unbreakable, bizarre cipher. Several major
events and military exercises that have taken place during
intercepts of w3bk3rn3l's radio transmissions back the theory
}
 
1....<|.|>.......19..............35........|*...$!........%"..:wq

==> BEGIN ENCRYPTION

>BOT

Estou em L73 faz já uma beca. Hoje, descobri como aceder a algumas
das mensagens que são veiculadas através do sistema Quantum
Entanglement Communicator, mais conhecido pelo QEC. No entanto, as
mensagens parecem chegar com muitos cortes. Vou dar-vos conta de um
pequeno fragmento que consegui intercetar:

"Hxmxs xamado ax sixtxma, X1. ¿Pxxden oírxxs? Paxxxe qxe no, nadxe
puede. Nos hemxs pexxido".

Penso que esta mensagem terá sido emitida por uma nave espacial
chamada Alsi Lotsir.

Estou ansioso por descobri de quem se trata.

[#$%&"/&&"%!$$$ --> noise]  

>EOT

