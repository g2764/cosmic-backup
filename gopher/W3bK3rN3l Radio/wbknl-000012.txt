```
        ___________.    __    ________              ________ .__   
__  _  _\_____  \_ |__ |  | __\_____  \______  ____ \_____  \|  |  
\ \/ \/ / _(__  <| __ \|  |/ /  _(__  <_  __ \/    \  _(__  <|  |  
 \     / /       \ \_\ \    <  /       \  | \/   |  \/       \  |__
  \/\_/ /______  /___  /__|_ \/______  /__|  |___|  /______  /____/
               \/    \/     \/       \/           \/       \/      
                   .___.__                                         
____________     __| _/|__| ____                                   
\_  __ \__  \   / __ | |  |/  _ \                                  
 |  | \// __ \_/ /_/ | |  (  <_> )                                 
 |__|  (____  /\____ | |__|\____/                                  
            \/      \/                                             
```
W3bK3rN3l radio outpost
wbknl-000012
-----------------------
KG-84 encrypted MIL-STD 188-110B
Message made on an ISB circuit
--> Croughton (AJE)
--> Sigonella (NSY)  

{
Several theorists suggest that this is a secret communication
station for a secret society, agency, or government, to be used
to communicate in an unbreakable, bizarre cipher. Several major
events and military exercises that have taken place during
intercepts of w3bk3rn3l's radio transmissions back the theory
}
 
1....<|.|>.......19..............35........|*...$!........%"..:wq

==> BEGIN ENCRYPTION

>BOT

Nesta mensagem, vou falar-vos um pouco de mim. Quando era
criança, chamavam-me o "robô triste". Eu morava na América do
Sul, num lugar cheio de montanhas limpas e ovais, sem picos
afiados capazes de perfurar balões e sonhos. Mais tarde, já
em adulto, fui para a Europa, mais precisamente para Moscovo, na
Rússia. E posso dizer-vos que a Rússia é eternamente fria.
Arranjei um emprego numa empresa que vendia móveis, na secção
de contabilidade. Mas a América do Sul nunca deixou de estar no
meu pensamento. Era lá que procurava  um refúgio temporário,
para tirar umas férias do deprimente e opressivo mundo onde cada
um de nós tinha de se aguentar como um trabalhador assalariado de
escritório.

Em Moscovo, consegui subir na hierarquia da empresa, até chegar
ao topo dos empregados de escritório. Já nessa altura, tinha um
fascínio especial por literatura. Lembro-me de ler Vila-Matas e
sonhar um dia poder plagiar os escritores famosos. Mas a minha
carreira de escritor falhado nunca chegou a começar, porque,
pouco depois de ter começado a escrever o meu primeiro romance,
sofri do chamado Mal de Montano. Foi aí que decidi abandonar a
empresa de venda de móveis e alistar-me nas forças especiais
russas.

[#$%&"/&&"%!$$$ --> noise]  

>EOT

