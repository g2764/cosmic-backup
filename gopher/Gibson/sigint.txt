Me están siguiendo. Estoy seguro de que me persiguen. Hace
diez manzanas. Por lo menos diez cuadras. Y no, no estoy
paranoico. No proyecto mi agresividad en los demás. No tengo
ideas fijas de ningún tipo. Caminaba por Ángel Gallardo, en
Villa Crespo, cuando noté a mi sombra. Se hacía el boludo,
solo le faltaba un anacrónico diario cubriendo su jeta para
ser más evidente. 
Quizá, Marcus, debería empezar desde el principio. Pero es
difícil dictarle a mi Ātman esta historia y subirla a tu
repositorio privado vía el NIST-4 ... a propósito, nunca te
felicité che, conseguir acceso root a un satélite del
National Institute of Standard and Technology, dejate de
joder, que maravilla. 
Me sigue persiguiendo. Ángel Gallardo ya se termina ...
¿agarro por Estado de Israel o giro por Corrientes? Nunca
fui tan capaz como vos de tomar decisiones sobre la marcha.
Querido, ojalá esto te llegue y me puedas ayudar ... voy a
intentarlo.

---
import socket as sk 
cliente = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
---

Joder, joder, no puedo Marcus, no puedo programar a la vez
que camino al mismo tiempo que le presto atención a este
hijo de puta. Me parece que ya ni siquiera intenta ocultar
que me está persiguiendo. Te lo juro Marcus, todo esto es
verdad. Sé que más de una vez señalaste que siempre fui
propenso a la exageración, a la megalomanía, a los delirios
de grandeza. Y que siempre estuviste en contra de que me
instalara este Ātman. ¿Cómo no voy a estar paranoíco,
para-noûs, verdad? Por supuesto que mi noûs, mi mente, mi
yo, mi alma, dado el Ātman, está por fuera de mí mismo.
Tenías razón, pero estuve todo este tiempo intentando
recuperarla, pensé que había conseguido algún avance y ahora
este mierda, esta bosta me persigue. 
Al final seguí por Israel, estoy por llegar a Córdoba. No sé
hasta donde piensa perseguirme este choto. Si llego a
Pueyrredon puedo meterme en el club de Case, "Gent Fablar",
¿te acordas? Y ahí quizá me puedo contactar con vos:
terminar de escribir el script, encriptar un mensaje urgente
y rezarle a Obbatalá porque te llegue a tiempo. Vos vas a
saber que hacer Marcus, siempre supiste que hacer.
¿Qué posibilidades hay de que un tipo de casi dos metros, en
un Perramus negro, durante casi 40 cuadras, camine
exactamente por el mismo camino que yo después de que
intuyera de que está detrás de mí? No hay forma che, posta.
No es otro episodio de psicosis delirante crónica. Si bien
nunca lo fueron, pero eso me hicieron creer ... y hasta el
día de hoy sigo dudando de mis propias percepciones, vos
sabes.
Tal vez debería evitar hacerme el inteligente, dejarme de
joder con ese script, y usar directamente netcat. Intentar
abrir algún puerto TCP, o mejor todavía un puerto UDP,
conectarme a la Eleggúa y ... ¿se está acercando? Mierda,
mierda. ¿Qué decía? Netcat, UDP. Concentráte aMok,
concentráte. 
¿Tendrá un Ātman este forro? ¿Se preguntará, a la noche,
como yo, si valió la pena haber perdido la noûs para ser
capaz de ejecutar instrucciones binarias a la velocidad del
pensamiento? Veo una torre de 6G. Puedo mandar un ping e
intentar averiguarlo. Si el cortafuegos de su Ātman fuera lo
suficientemente primitivo, en una de esas abro un puerto con
netcat y le calcino la mente. Pero no me animo. Tengo miedo
de fallar y que después todo sea peor.
Estoy en Av. Córdoba, al 3600. Diez cuadras más para llegar
a Pueyrredon. ¿Qué es eso? Joder, joder, mierda, rápido,
traspasar los protocolos

---
Session Connected
---

"Subí todo sin encriptar y sin ... Marcus, argh .."

---
Session Disconnected
---
