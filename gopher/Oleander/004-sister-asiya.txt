REC ON
TRN ON
ENC FAILED
SYS GOOD
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Entry 4 -- Sister Asiya

I am Asiya, called Choden in Hazen, Tashi among the drokpa, named
Lobsang by the lama after the eighty-second of his name, and this
is my story. I give it to the stars that you might know me and
know our plight. If I succeed, let this be my testament. If
I fail, may my words carry the mantel to your shoulders. The Jinn
are enemies of all life. They are your enemy, my sisters.

My village in the southern mountains of Hazen's Emirat continent
was called Shannan Karpek. We were far from the influence of the
emir and lived the old ways. My brother inherited our farm when
our bubākō passed. He was a devout child and grew to become an
honest man I was proud of, my Utkrishta. He met a muslima while
trading and they were to be married with the next harvest
celebration. This was not unknown in Shannan Karpek where we all
understood the Buddha and Allah as the faces of a coin. I was
proud of my brother, as were my sisters and our cousins.

Through all of this I was a herdswoman, a shepherdess, and my kyr
were lush and beautiful. I was too young still to be married, but
already I knew my passion in flesh was not for men. This also was
not unknown in Shannan Karpek. My brother loved me, as did my
sisters and our cousins.

The season of growing and birth was upon the land when the Jinn
came to Hazen. We saw nothing of this at first being so far from
the cities and star ports. Even when word reached us of the
fighting and the dying, our elders spoke with sadness about these
things happening far away. The Jinn had no reason to come to
Shannan Karpek, no reason to take our kyr and our wheat. They flew
in cities among the stars. They ate stardust and the light. They
could create wonders and destroy cities with ease. We were safe.

My brother's cry woke me in the night. It was sharp and high, the
sound he made as a child waking from a nightmare. But he was not
a child any longer and my fear grew. So quickly roused from sleep,
perhaps I still had something of the dream upon my eyes, or
perhaps I knew something terrible had come deep in my heart.
I know that I was a coward then, whatever the reason. My brother
cried out and I froze. I did not go to him. When other voices
began to join his, when the village was filled with screaming,
I did not go to them. I hid.

I crawled first beneath my bedding, and then into a corner of
a storage cellar under our flooring. From my hiding place,
I listened to the suffering of my family and friends. I listened
as they were gathered up and torn apart, one after another. Men
torn to shreds, women in pieces, even the children. My people were
butchered like kyr at market. The screams. The endless screams. My
people took days to die while I hid. Between sobbing I ate stored
goods in the crawlspace under my hut. I stayed in fear and shame,
lying in pools of my own waste waiting for those screams to end.

My shame. It knows no limits.

The Jinn are not like us, sisters. They do not have hearts of men,
though they may look like men when they choose. They are not
created as we are, called toward goodness and family. They do not
battle and war to preserve what they have or even for conquest of
land and goods that they need. They do not make war, they make
havoc and pain. They butchered us in the mountains, they butchered
us in the cities. They left one village untouched for every twenty
ruined, driven into dust. That village they visited and gifted the
meat. Those poor, poor people. They... they were forced to accept
our meat.

The Jinn knows our ways and our customs. They know our souls and
they know how to injure us there at the core of our faith. They do
not seek to kill us, they want us broken. They watch as we rot
from the inside.

I am a coward who survived the massacre of Shannan Karpek, who
survived the ruin of the Emirat, who survived the consumption of
Hazen. There are others who lived, but none of them survived. I am
alone in my shame and honor.

These women with me share that burden. We are what happens when
the pick strikes a stone in the field. The earth may be tilled
again and again. Then, with a suddenness that can only come to
those who are assured of their control, a piece of flint. The
sparks will fly soon. They will seem small in the vastness of
space and time, but we have a secret.

We know the Jinn. We now know their ways and customs. We know
their souls, or lack thereof. We do not seek to kill them. We will
break them so fully that their rot will consume them.

I have no life remaining but the one they gave me that night in
Shannan Karpek. I have no path left but the one they set me upon.
All of our Gods have seen what will come. The Jinn have none to
warn them, so I will do it myself.

I have shared with you in the clear. I do not hide my story from
the Jinn or from my sisters. It will not save them from what is to
come. Let them burn with my words upon their lips.

To my sisters--

                            EMAHO

              No-tsar sang-gyä nang-wa ta-yä dang
              yä-su jo-wo tug-je chen-po dang
              yön-du sem-pa tug-chen tob-nam-la
              sang-gyä chang-sem pag-me kor-gi-kor.
              De-kyi no-tsar pag-du me-pa-yi
              de-wa-chen-she-cha-wä shin-kam-der
              dag-ni di-nä tse-pö-jur-ma-tag
              kye-wa shen-kyi bar-ma chö-pa-ru.
              De-ru kye-nä nang-tä shäl-tong shog
              de-kä dag-ki mön-lam tab-pa-di
              chog-chu’i sang-gyä chang-sem tam-chä-kyi
              geg-me trub-par chin-ji-lab-tu-sol.
              TAYATA BENTSA DRI AWA BODHA NA YE SOHA

                            Asiya

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
