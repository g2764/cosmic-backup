--------------{PING}-------------
.MAX TIMEOUT of   18 days reached
.RECONNECTING 
--------------{QED}--------------
.CONNECTION ESTABLISHED
.PARSING LOGS
----------{SHIP STATUS}----------

           ' ' '  
        `  . … .  ´
      ` _~       '_  ´  
       /    (|)    \  
      |______|______|
      Ä     /D\     Ä
      :  /N | | …\  :
      ·  \… |_| V/  ·
      .     \K/     .
             |
             Ö      
     .       °       .
             °                      


     Crew: 
          *K-Pashar   Active,Rear
           D-Operator Active,Point
           V-Operator Sleeping
           N-Pashar   Sleeping

     Ship: Shields 100%:0%, Accelerating at 1G (day 12/17)

K Reports: Systems are green. All crew have acclimatized to their
           private quarters. No issues to report.

K Remarks: D is a good operator. We've not deviated even a pin drop in
           the last few days. Good sense of humour too.