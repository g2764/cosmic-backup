PERSONAL LOG, ENTRY #2505
AUTHOR: Malcom D.

After an apparently eternal journey, we finally reached
Rqtr.  This outpost is amazingly large, and the cost for
renting an apartment is huge!  Fortunately SVERSYL is well
equipped, and we can basically use it as it was our home.

We took a good look around.  There's a lot to see and to do,
and many shops where merchants sell interesting products.
It was not easy on Fnynzr to find Earth artifacts and such!
Of course they are quite expensive.

The local administration did an amazing work to make this
outpost "Earth-like", and it really looks like walking in a
Earth city.  Sure, from time to time you get dazzled by some
flares reflecting on the dome.  But that's not a big deal.

We have a couple of days left before I finally get to know
what the job will be about, so we can happily waste some
time and check out this new place.  Of course we cannot
spend much for the moment.

EOF
