Isla Ristol

Entrada 22

Hemos visto Deimos muy claramente y es muy interesante
para ser una callao de playa gigante. Obviamente no lo
hemos visto a pelo sino con la cámara astronómica de la
nave.
 
  No impresiona mucho, la verdad, pero hay una estación
allí de proyectos científicos, casi siempre administrada
por robots, aunque también hay una base de emergencia por
si te tienes que quedar o mandar algún técnico a quedarse
alguna temporada. 

  Está en la zona de Deimos que llaman "la sombra" porque
ahí nunca llega el viento solar (directamente) y puedes
sobrevivir más tiempo. (Sé que es un poco tonto porque 
la base de emergencia debe ser al menos tan segura como
esta nave espacial, pero me da pena pensar que alguien 
tenga que pasarse tiempo allí).

                 ··---··---··---··

16 días para Marte, 

*************
  
~ Enteka (enteka a-rarrita fastmail aquiunacosaita com)
