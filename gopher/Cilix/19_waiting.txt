
I got outta there as fast as I could.  I ran back to my hotel room
and here I sit, recording these messages.  All I can do now is wait
for the end to come.

Waiting seems pointless when you already know how the ending goes.
I've figured it all out now Judy.  You were never after the triple
indemnity insurance money.  I should've guessed that.  It's so
rarely paid out.  Those clones are damn loyal.

No, you weren't after the money.  You just wanted out.  They'll
arrest me, charge me with murder, and the whole triple indemnity
clause will be as void as the dead space that envelops this icy
moon.  You'll get a new owner, another rich guy probably.  I
should've seen that sooner, you /had/ money.  With people, it
usually comes down to money, we'll do anything  for it.  But,
you're not a person, are you Judy?  For what it's worth, to me you
were more of a woman than any I ever met.

Were you just bored with Walsh?  Do androids get bored?  Or is
there something more going on here that I still don't see?  You'll
just end up with another Walsh, maybe on a different moon of
Jupiter.  Maybe somewhere even farther out.  I hope you finally
make it to Earth.  It's a nice place, when you compare it to here
at least.  I probably won't ever see it again.

Somewhere deep down I still believe you really loved me.  That you
were counting on me to not screw up and get myself caught.  You
would've gotten that triple indemnity payout and we would meet
again, as strangers, in that same bar.  Maybe we would be sipping
those same drinks with that same ice cooling them, the ice that can
only be found deep down in the core of Europa.

--- EOF ---

