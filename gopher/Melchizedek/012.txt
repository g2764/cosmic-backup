Message Incoming...

Source Melchizedek.0294
Approach β Hyi
Ascension 00h 25m 45.07037s
Declination –77° 15′ 15.2860″
Distance 24.34ly
Equinox J2000.0 SOL
Year 3782, QEC adjusted

[Autotranslator enabled...]

Jerome Somerset Pasani, Warrant Master
:::
I am serving the following packet in parsable envelope over QEC
for intercept and transmission to the Idjani family. Due to our
time dilation, I am not sure this will reach his father,
descendants or ancestors. Local data-authority intervention
requested. If anyone out there can help get this into the right
hands we will owe you a debt.

ENVELOPE 001 START
:::
Dear Mr. Idjani and extended family of Moussa Idjani:

I regret to report that Doctor Moussa Idjani, ship physician to
the Melchizedek and a personal friend, is deceased.

Men of your son's utter dedication to the cause of life are rare
and the loss of such a man is humanity's loss.

In the years of close confidence and friendship that has grown
between us, I have ever been impressed by the strength of his
spirit and his cool confidence in the success of our program. He
was a leader who shared his strength and faith with all who knew
him. By his courage, skill and dedication, he has guaranteed
future generations a place in the universe and a chance for peace.

The work we do and the role Moussa played in it can not be
overstated. It is to him that we owe our lives and hopes in the
future.

Your sacrifice is beyond measure, but I hope you can take some
comfort from your knowledge that your pride can be without limit.

I mourn with you as we pray for God's blessing.

Millions share our debt to you for giving Moussa to man, and
inspiration to mankind.

Jerome Somerset Pasani, Warrant Master, Melchizedek Covenant Arc

ENVELOPE 001 END
:::
I have, much to my great pride, never in the past been faced with
a situation of having to report on the death of one of my crew.
I admit to have borrowed heavily from history for the format of
the letter. I hope it offers his family some consolation in this
dark time.

What I cannot offer them is an explanation for what happened.
I have watched the video recordings, listened to the QEC
transmission, interviewed the crew, and all that remains is more
questions.

Prezzi would suggest a logical review: carefully considered inputs
and value measurements, a systematic approach to variable
limiting, and absolute, brutal precision in analysis. Had I her
training from the Ecclesia, I would use it to cut loose, to stop
seeing his face in my dreams, and to stop the incessant questions
nagging me. Why didn't I agree to wake Kroups? If I had, would he
be gone instead? Why did I allow it to begin?

Moussa was my conscience and confidant. He knew this didn't make
any sense, but I pushed him regardless. We're so close to Beta! We
just need to stretch a bit further and we've done it, the
impossible task. But the cost.

Moussa was my friend. I owed him more.

I owe his memory more.

At 24.542041, our doctor was attacked by a mutated arabidopsis...
Entity. The violence of it--it began in what appears to be
a reaction to an ultrasonic probe making contact with a small cell
sample. The sample was in isolation in a mechanized microscopic
recording device called a SAM3 (I'm not sure what the designation
stands for). The remaining arabidopsis was contained in a secure
section of the fore-botany bay, isolated by translucent graphene
sheeting and reinforced self-sealing biofilm.

The moment he--the moment it happened the separate arabidopsis
plants, individually, broke through the secure enclosure and
attached themselves to Mou--Doctor Idjani's unprotected skin
around his hands, arm, and face. The plants moved independently,
and with incredible force. The graphene sheeting alone can
withstand nearly as much force as the hull crush-plating. Our food
production requires the utmost care and safety, which it seems
did us no good at all.

I don't know how to describe the movement. I've been watching the
video on loop wishing the QEC had the bandwidth to encode it and
send along. I imagine there will be those back home who won't
believe the reports no matter how carefully I present the
evidence. Plants don't move like that.

Arabidopsis is a common plant, or was when we left on our mission.
For those of you who are not familiar with it, it is closely
related to spinach or mustard greens. It grows well in a wide
range of conditions and environments. Those aboard the Shin-Salyut
replica orbital station may be familiar with it. If memory serves,
it was the 6 or 7 Salyut that flowered the first arabidopsis in
space, making it the first human space-plant. It flourishes today
(or when we left) on the lunar colonies as leftovers of the
original Chinese bases. I believe it has been adapted to Martian
soil now, and it is one of the standards for botany labs on
Visscher craft.

I've spent a lifetime with this plant, and whatever the hell
killed Idjani was not that. Something changed when we were in
cryo, and the DNA pasting doesn't account for it. The plants, the
way they moved, it reminded me of Cephalopoda. I've seen videos of
the aquatic creatures when they reach land and fling themselves
around with flailing limbs. Arabidopsis, or what used to be
arabidopsis, moved like that. It was animal movement and unnatural
to witness.

The plants shot across the room so fast, the camera had to be
slowed to see it clearly. The graphene just crumpled inward and
they piled through. There are scrapings of plant remnant on the
edges that were identified prior to purging the compartment. The
things didn't seem to care that they were shredding their
own...flesh...bark? I don't know how to classify any of this. We
need to come up with new terms. A-rabids? I don't know.

Anyway, as unnatural and difficult to explain that part is, what
comes next challenges us all. If Eva hadn't witnessed it
first-hand I don't know that I'd trust the cameras, frankly.
There's only so much you can take in at once.

The arabids attached to Moussa's skin. This part I watched
clearly. They didn't touch a piece of clothing in the act. They
knew exactly what parts were him and what weren't. Almost as soon
as they connected, the buds sort of burrowed into him. The growth
rate was inconceivable. A whole root system shot through him and
sort of affixed him to the spot. It was like someone told him to
freeze in place and he acted like a tree, except he was a tree.

The image of it won't leave me. I know Eva feels the same. She
can't go near botany anymore. I can't blame her.

Eva Hämäläinen deserves some mention here. I believe our situation
would be significantly more dire without her immediate action. She
has asked that I forgo sharing some of the details of the event
out of what I believe to be embarrassment, but I must decline. To
leave anything out of the encounter is a disservice to her bravery
and undermines the intensity of the critical moments.

As the arabids affixed and rooted into Moussa, they did not kill
him instantly. As best we have theorized, the root system was
attempting to work symbiotically with his own circulatory system.
While he was unable to move or communicate as normal, he was very
much alive. The recordings show him struggling against it at
first, but after a few minutes he seemed to settle into place. We
couldn't make out the words clearly beyond a reconstructed phrase:

"Together we grow, together we grow, together"

He repeated it softly as Eva arrived. She saw him and what it was
doing, and it...saw...her as well. You can hear her reaction on
the QEC as she saw the scene. Eva screamed like a banshee and
vomited simultaneously. The later I mention despite her
reservations because of the importance to her own survival. The
surface growths on the doctor moved toward her as she screamed.
They were so fast, I believe they would have reached her before
she could react. However, the arabids did not seem to be able to
distinguish between her and her vomit. The plants splashed down
onto the floor for a brief moment, giving her time to act. The
emergency containment and isolation systems were already in place,
but breached. Melchizedek's systems were on high alert, so when
she pushed the hot-release button, the whole fore-botany container
detached instantly.

Were we in normal space and not under gravity-sheer, Eva would
have been pressure ejected into vacuum between the plates.
Instead, the atmospheric isolation was enough to hold her position
as the emergency crush plating dropped into place. Fore-botany hit
the grav-shear wall a moment later and was broken into subatomic
particles instantly.

We had no opportunity to examine Doctor Idjani's body or any of
the affected plants in fore-botany. The doctor's active samples
were all included in that compartment when it jettisoned. Despite
the value of the food stores and the scientific questions raised
by the events, I am glad it's over with. This nightmare cost me
the life of my friend. We must proceed without our doctor. Our
questions have no answers: neither scientific or from faith.

Prezzi would suggest I proceed logically, and I will do my best.
The food shortage is our most dire problem and we're unequipped to
deal with it. I'm thawing out Xavier and Kroups immediately.
I need fresh minds who know the science to get us through this.

We will get through this. Humanity is counting on us. We have paid
a dear price but it will not stop our mission. I will be as clear
as I can to all who read this whether traveller, scientist, or
crew: I mean to seed Beta Hydrii with the gift of life, the
blessings of God, and make a home for our people in the stars.
I will see that through even if it costs us every life aboard. We
will pay the price.
.
