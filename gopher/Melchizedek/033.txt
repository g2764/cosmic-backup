Message Incoming...

Source Melchizedek.0294
Near β Hyi
Ascension 00h 25m 45.07036s
Declination –77° 15′ 15.2860″
Distance 24.35ly
Equinox J2000.0 SOL
Year 3782, QEC adjusted

[System encryption enabled.]
[Autotranslator enabled...]

Stephanie Janssen, Specialist First-class
:::
To all Earth vessels: we've taken command of the Melchizedek.

Our captain, Jerome Pasani, is missing. Our crewman, Sandy Kroups,
is missing too. We are going after them and we're gonna bring them
home. Or to Beta Hydrii, I mean. New home.

Our Seriph Rhetorical turned her back on them, so we've removed
her from command. There's twelve of us on board:

- Stephanie Janssen (Me)
- Eva Hämäläinen
- Dr. Spinelli
- Dr. Reed
- Hildar Simms
- Maureen Hendrix
- Paul Wall
- Sandra Gould
- Lillie Cleveland
- Donna Fernandez
- Alonzo Alvarado
- Edgar Marquez

Paul, Alonzo, and Edgar are here under duress. Is that the right
term? They didn't want to be on the ship when we took it, but now
they're stuck with us. So note that or something in case we get in
trouble later. We're almost certainly going to get in trouble
later. If there is a later. There's almost certainly going to be
a later. Probably.

Eva is navigating us back along the path that Sandy used to get to
the anomaly. Also, we're calling it the anomaly now. I suppose
it's good to have names for things.

Eva said that we don't need to actually follow the whole path that
the shuttle took last time since we know they didn't run into
anything for a while. They read out their position when they got
anomaly'd. Anomalized. When they saw the thing. So we're going to
cut across the diagonal, kinda. It's still orbitals so there's
curves and parabolas, but we're cutting across them too. It won't
take us as long as it could have, but we'll still be out here
a bit. That's okay. We have food and water. Maureen fixed that up.

The night before we left I was hiding in support girding above the
fore gangway, sandwiched nicely up against some venting and
coolant tubes. If we'd been flying that would have gotten pretty
chilly, but instead it was just boring. Meanwhile some of the
others were being amazing. Maureen changed around duty rosters and
tasking so that the crew unknowingly re-loaded a bunch of stores
onto the ship over the past couple days. On the one hand I can't
believe no one noticed, but really, why would you question it? The
crew gets all assignments to their pads and everything is
monitored and orchestrated so you're helping each other without
really seeing the big picture. Or not, I guess. Score one for us.

Score two--or goal? I'm not big on sports--was all Eva. Whatever
is out there it messed with DNA. It actually messed with Simms'
DNA in particular, and he's been eager to return the favor. We
have a lot of equipment that deals with sequencing, replication,
and dispersal. They're important for seeding. Well, that's an
understatement. They ARE the seeding. Them plus a bit of code and
bots, anyway.

It's like this: suck out all the genetic material you can, give it
a good mix, replicate it until you have a LOT, then force-mutate
it along pathways tried and true back on earth. You spray that all
over with a little secret sauce and, whammo, life. That's the
basics anyway. It gets you the microorganisms you need to support
later larger life-cycles. That's why we go in phases. Regardless,
the machines are important, so we have a few extras and we have
manufacturing to create more.

Manufacturing is its own tricky thing. The factories are designed
to self-replicate and to replicate any shipboard equipment. Given
enough time and raw materials they can duplicate the entire
Melchizedek, but just the mechanical parts. Rare elements are
still rare on other worlds, so finding Neodymium or something like
that is not easy. Fuel for the grav sheer isn't limitless, if you
see what I mean. But local craft? Oh yeah, we can make a bunch of
them.

During the early phases of seeding the factories self replicate
and replicate the genetics equipment as priorities. Each factory
has some math on how many other factories it needs to make before
it switches to the lab equipment and so on. They grow like fungi
or spores or something. It follows one of those Greek letter math
things, like Pi. I forget which one.

Why all the background? Well I wanted to explain because WE STOLE
STUFF! Specifically we have a manufacturer unit and one each of
the genetics units. More than that, we set them up to actually
work on board. Oh yeah, I forgot to mention that part. Normally
they're for planetside work. Well, not anymore. Eva snagged us
some and then she FRIGGIN BLEW UP THE HABITATS! That was wild.

Lillie and Donna arranged a film night in Habitat D. Have you ever
seen Double Indemnity? It sounded cool, but I missed it. That got
the majority of the crew off duty out of the other habitats. Then
Eva planted explosives in them and in a bunch of the other
processing plants. I didn't get it at first, but once everyone was
aboard the Melchy it started to make sense.

See, starting up a starship isn't a quiet or fast process.
Especially from the ground. It's loud. Like explosion-level loud.
It shakes the ground and smoke is everywhere. There's also a lot
of comms traffic that goes on around it. Most importantly, Prezzi
could have used her access to get onboard before we could make it
into the air. We needed one hell of a distraction to steal a whole
ship. Eva is amazing.

The habitats were easy targets since everyone will freak out about
their stuff going up and immediately run to see if they can save
things. The other explosions hid what we nabbed from the ground,
just in case they have some way to try to stop us remotely. Nobody
was too clear on that point. Shhh.

So once the explosions started, those of us who were onboard
needed to secure controls. That was probably the easiest part.
I've been lying in wait just behind the control room entrance for
two days. Once they all stepped outside I just dropped down,
scooted inside and locked the door. Ninja moves! Do you know what
Ninjas are? We saw an old film about them too. Oh, and the doctors
rounded up the others on duty and kept everyone quiet until we got
airborne. Almost forgot to tell that part. Dr. Reed is strong!
Wow.

Oh, if you're worried about the crew back planet-side you don't
need to be. There was a lot of damage done that they'll waste time
fixing over the next few weeks, but nobody was hurt and they're
not in danger. Probably. I mean, we haven't called them, but from
the reports I saw they should be fine. Prezzi will be really
angry. Or no. She'll be very logical and cold and give dirty
looks, which is about as angry as a Seriph can be.

We'll document more soon. Hopefully we can talk Alonzo into
helping with the genetics research. That's his field. In the
meantime, lots to do.

Peace and love and righteous disobedience to you all!
.
[End of encrypted envelope.]
