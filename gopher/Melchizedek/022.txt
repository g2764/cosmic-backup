Message Incoming...

Source Melchizedek.0294
Exiting Sol
Ascension 00h 25m 45.07036s
Declination –77° 15′ 15.2860″
Distance 0.01ly
Equinox J2000.0 SOL
Year 2183, QEC adjusted

[Autotranslator enabled...]
[Personal Log Entry]
Jerome Somerset Pasani, Warrant Master
:::
Hi mom,

This is really happening. I've been on the long-haul transport
racket for nearly a decade but nothing could come close to what
we're about to do. We're all trained as well as we can be and
they've stacked this bucket full of geniuses. I told you Moussa is
coming with us too. It'll be great to have a friend along, to know
someone on the other side of all this.

I just can't believe I'm actually saying goodbye. It feels unreal,
like it's happening to another person. We're no further away than
I've been before but once we kick on the grav and slip into those
coffins that's it. You, dad, Lisa... I knew I'd never see you
again, you'd never see me again. We talked about it, you know
I believe in this work, that it's worth it. It's worth my life,
all of our lives. It's worth a planet and more because we're
saving life itself. But I can't lie, I hate it too.

Dad was a true stoic at dinner. I know you were embarrassed by
what happened, but I don't blame you. I wish dad acted more like
you. I was a blubbery mess last night when I got home and I sat in
the oxygenator for ten minutes this morning to get rid of the bags
under my eyes before coming aboard. I'm just going to miss you all
so much. The words don't do it. I love you. I love you all so
much.

I don't want you to mourn, please. I know what dad said, but it's
not like that. I'm not gone, I'm just far away. Your son will live
far, far into the future and you can always feel good knowing I'm
out there somewhere doing what's needed. When your days are up and
you're ready to move on I'll be out there still. You can take some
solace in that and never have to worry or fear. When I wake up
I'll do my duty. Each and every day I will wake up and thank God
for such a wonderful family and such caring, loving parents. Yes,
loving. I don't care what we went through or argued about growing
up. I'll thank God for you every day, I swear. I'll go on and live
my life and do everything I can to keep on living, to keep life
going across the galaxy.

We're agents of the almighty, right? That's what you taught Lisa
and me in Sunday school. We're little temples that move, holy
islands acting as agents for God. Well your holy agent is doing
his best and he will continue.

Don't mourn, please. I'll be sleeping for a while, that's all. And
when we wake up I'll make you so proud of me. I have to. This has
to be worth the cost.

Oh mom, how can I do this to you?

You've been through so much.

I have to do it, but you never volunteered. How can I take that
pain away? Even this best of actions is causing you pain. How can
I come to grips with it? How can that be justified? We've got
a Seriph on board. Maybe I'll ask her. Or maybe not. She's likely
to slap me for being too emotional or something.

Oh I wish you could have met everyone before we set out. Maybe
that would have made it easier, knowing them, knowing I won't be
totally alone. But then, what am I leaving you with.

If dad isn't there for you then please call Lisa. She'll talk to
you and you can help each other. She knows about this work, she
can even explain how we do what we do. It's not all machines and
computers, I swear. It's microbes and onions and human hearts.
It's all the good of God wrapped up in a bottle we're flinging at
the stars. Isn't that beautiful?

Please, Mom. Don't grieve.

I really hope this letter doesn't make things worse. I just want
you to... I don't know. I want to take the sadness away and fill
you with hope and joy and pleasure knowing that I'm doing what I'm
called to do. But maybe that's not something I can just tell you
and have it work. It took me such a long time to understand this
calling in myself, how can I expect you to get it from a letter
while you're dealing with the loss of a son.

This is too much for today, I think. I'll set it aside for a bit
and send it later. Maybe I can get the QEC to relay it when the
time is right and you've had a bit of time to heal.

I love you, Mom. And Dad too. Give my best to Lisa and tell her
I'm counting on her. She better take good care of you two.
.
