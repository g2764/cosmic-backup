Relief *sighs*. We are safe. No fatalities!

Jeez that was tense. The entire crew panicking, cursing, it was a
madhouse.

It seemed that once we reached the center of gravity (of the unknown
mass) it just... disappeared? Anyone have any ideas what this was?

But anyway, we're through that weird thing now. Attempting to
repair the ship. Hopefully, we'll be fully online and ready to roll
with mapping in a few weeks. There may be a few parts that need 
replacing, luckily we have a person who is good with electrics and
should be able to put together an ad-hoc solution that will last
until we can get to another ship/port.

Our main concern currently is that the main computer is fried, but
hopefully that isn't the case. At the moment everything is operating
independantly of each other part.

Oh, this also means no Automated Reports for a while.

P.S. The relic is still lying in the cupboard. Everyone's been too
busy to look at it.

Any help would be damn appreciated.
