-+-+-+- Regular Report -+-+-+- C98.204 -+-+-+- Orange -+-+-+-
Dr. med. Ursula Hägi reporting on the second scheduled inspection. The
passenger status in cryo sleep is nominal. We have had no failures. My
companion for this round is Dr. phys. Hans Peter Frey. We've spent the
first two days reviewing the logs. Today Hans Peter found that the
cryo programming has been manipulated. I think he's a bit out of his
league. I tried helping out but in the end we just set the ship AI to
work. We're training an adversarial security network as we speak,
hoping to quickly isolate the misbehaving parts. The problem with this
approach is that we won't be able to learn what the saboteurs had
intended to do. Terrorists? Down-Earthers? Sometimes I wish we had
better military personnel on board. But who am I going to warm up? No,
we'll just let the ship work on it and if we get results in two days,
a warning for the next inspection team will have to do. Peace.
-+-+-+ End of Report -+-+-+ HAEUR -+-+-+ 
