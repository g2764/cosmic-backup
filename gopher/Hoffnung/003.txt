-+-+-+- Social Report -+-+-+- D33.112 -+-+-+- Orange -+-+-+-
Samuel Hochueli speaking in the name of the free anarcho-communist
commune-ship Hoffnung. We hereby declare our independence from
shareholders and all contracts the previous owners of Hoffnung entered
in. We consider these to be null and void. The resources of this ship
have been claimed by the people, for the people. The regular crew you
employed continues to sleep safely in its cryo sleep. While they
sleep, we woke early and plan the new future for TRAPPIST-1d. We
reject the old plans of a shipping outpost, a refueling station, an
industrial hell hole; instead, we proclaim intellar siblingtude with
all native life in the One ocean. We shall build a new society, free
from late stage capitalism. Philomena Auerbach has redone our recent
nav-vectors and we shall land the ship on TRAPPIST-1d. Do not expect
the promised refueling station when you come there. Instead, we await
new settlers willing to join the first interstellar utopia. Welcome.
-+-+-+ End of Report -+-+-+ Signed SAHOC -+-+-+ 
