++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++  TO: <HQ Dept of BI>BI@BNL.com
++  FROM: <Captain Mike T Robinson>MTRobinson@BNL.com
++  MESSAGE RECEIVED.
++  AUTOMATED RESPONSE: AMENDED.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

It has been 400 years since your last transmission, and AUTO 
wouldn't even let the Captain read that one. I would like to take 
the opportunity in this period of renewed communication, to 
respectfully ask, as a fully vested member of the board, just when 
will Earth be habitable again?

Operation Cleanup was only supposed to take 5 years! Despite some 
light bone loss, our crew is more than ready to begin Operation 
Recolonize. But instead of finishing the mission you corporate 
brains come up with Quantum filing cabinets? At least AUTO let me 
read this one, but seriously?  We can already transmit between 
ships. I just got off the horn with Captain Brace of the Axiom to 
confirm that this isn't some sort of a sick joke. Who's gonna use 
this "QEC"? I can't help but feel this is a waste of funding.

Respectfully,

Captain Mike T. Robinson
Assumption

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++ END MESSAGE
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
