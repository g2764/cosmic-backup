We've just received word from the Galactic Federation; there's
an unknown object on collision course with our planet. They
will send a ship to pick the people on this planet up and any
ships we have.

This planet is pretty poor; we only have one or two ships per
community. I live on my own, so I have my own ship. Entitled
the Enterprise, it has two beds, with shelves in the walls for
each person. On the other side of the living quarters, there's
two shelves for multipurpose use. I currently use some of it
for a tiny hydroponic garden. At the back of the ship is the
food/cargo hold/emergency escape pod.

The cockpit is outfitted with an 80x80 terminal, which has a
framebuffer. I've outfitted it with my home built QEC, as
it's apparently called. I found a 3 gigabyte RAM chip, so
memory isn't a problem. The only inconvience is that I cannot
save anything. But I can still get all the published texts
from the relay station, so there's a plus there.

Still awaiting an estimate for the collision; for now I've
been farming and scavenging for food, and moving things to my
craft.

Until next time,
