-- MESSAGE START ----------------------------------------------------
SECKEY:jxQA6hMXG%3#r9nfh%tdxo!REj%uGz
.......Accepted

Date:                        _____     _        ____               
EY 2265.04.23.15            |_   _| __(_)      / ___|___  _ __     
                              | || '__| |_____| |   / _ \| '__|    
Ship:                         | || |  | |_____| |__| (_) | |       
Garnet Star                  _|_||_|  |_|    _ \____\___/|_|       
                            / ___| _   _ ___| |_ ___ _ __ ___  ___ 
Location:                   \___ \| | | / __| __/ _ \ '_ ` _ \/ __|
8.26y out, sys LTS-1483      ___) | |_| \__ \ ||  __/ | | | | \__ \
                            |____/ \__, |___/\__\___|_| |_| |_|___/
User:                              |___/                           
TIMFLETCHER44@TRICOR                

:: BEGIN ::

It's always a hard call, deciding if it's safe to pull the family out
of cryo. I was fortunate that the AI determined this anomaly to be
minimal risk and didn't prio-thaw me. I've heard horror stories from
Kell, the Lead Scientist aboard. His first tour out to Bernards
Star.. the things that went wrong on that bucket. It's why I chose to
sign up with TC, they pay less than everyone else but at least they
spare no expense on the goodies on board. With this being an 46.2y one
way trip, I feel much better knowing everything is state of the art.

Oh right, my family. Everything is tip top, so I initiated the thaw
about four hours ago. Just ten to go until process is complete. If
I'm going to be out for four weeks, I'd love to have them with me.
In my spare time I'm still looking into cryo optimizations. There's
got to be a short cut between wake cycles that the system could
handle.

Anyway, part of the reason I'm sending this; anyone else running into
the same issue? Our onboard long-range sensor array is giving us data
on LTS-1483 that is very, very different from what the probes sent
back. Granted it was over 35 years old, but still. How did it miss an
entire planet? All the orbits are different from what we thought.
Kell is sweating bullets that III (our intended home) is off enough
that it may be not viable.

Not much I can do about it. Definitely no reason to wake the Captain
or Governor yet; we're following our protocols for this. Worst case
scenario, we go back to cryo, and when we're closer we wake again and
use closer scans to determine if we go to one of our other choices.

Tim Fletcher
Chief Engineer @ Garnet Star
=TRICOR034615734=

-- MESSAGE END   ----------------------------------------------------
