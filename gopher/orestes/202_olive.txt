===
Author: Olive
Date: May 30 2187 (Earth Standard Time)
Location: Orestes Outpost
===

Hello? Is this thing working?

Seems like it's some kind of protocol for communicating with rest
of the universe. Hmm...what should I say to you?

I discovered my parents old transmissions buried deep within the
file structure. They've only sent two messages in the past 17
years. They mustn't have much hope of being rescued. Not that they
let on. They always tell me the opposite. They tell me you're
coming to get us any day now. Whoever you are.

Of course, I stopped believing in that a long time ago. I still
pretend to have hope for a rescue, for their sake. Actually, I
don't really care if we're rescued or not. We have all we need
here.

I read over my mother's last message many times. I already knew the
whole story about her. My dad told me everything.

I read it to remember there was a time for us, for our family,
before the crystals. After that last message, my mother went right
back to her fascination (obsession?) with the crystals. She's still
my mother, and sometimes she feels so present and loving, but
mostly she's just a bit 'out' of it. She wanders around aimlessly,
talking to herself. When she's not zoned-out, she's asleep.
Sometimes, she sleep-walks. Sleep-walking on an outpost like this
isn't really the best thing. My dad was terrified she'd do
something to compromise our little artificial environment, so she
agreed to let him tie her to the bed when she sleeps. It's not a
pretty sight, but it's better than alarms going off randomly at
night.

As for me, I don't sleep much. I have nightmares. I feel like I'm
awake, that there is someone else in the room with me, and I'm not
able to move. It's some kind of woman who's there. Her face is
always in shadow.

For a couple of years, I would help my dad out at the dig-site, but
it started feeling too depressing. He's the only one who still goes
out there now. He's gotten pretty deep. Still nothing to report on
that front.

Now that I've discovered this old messaging system, I'll try come
back here again and write something else. It seems to be a nice
cure for insomnia. I'm starting to feel sleepy now.

The other night I dreamt Navy was in my room. His presence felt
different than the woman's. It felt comforting...exciting even. He
mostly ignores the rest of us at this point, so I don't know much
about him. He's a bit weird but he's always been nice to me. Since
the crystals grew back, he has returned to his 'study' of them.
It's a shame he and my mother don't join forces and actually figure
out what they are.

===
Sent via the QEC

