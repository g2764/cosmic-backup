===
Author: Green
Date: May 23, 2172 (Earth Standard Time)
Location: Orestes Outpost
===

It's been two years since Ruby's departure. I thought it was about
time I returned to this dusty old terminal and hacked something out
to send into the void.

We panicked for a while after she left. We were confused about
where she would have gone, how she went. Well, let's just say that
I was confused. The other two went off the rails. They weren't
worried too much about Ruby, only about their precious crystals.
Once again, it's been a living hell sharing this little rock with
them. If I didn't have Olive keeping me sane, the whole place would
have fallen to pieces.

Olive is two now. She can say "Daddy". She is resilient. She needed
to be.

Something happened recently that finally pulled us together again.
We discovered a stash of mining equipment, along with a broken
drone rocket, all sent from Arsinoe. It turned out they were
getting some of our messages after all. Fuckers.

Anyway, we were angry when we found out about this little delivery
that was so malicious kept from us by our A.I. I mean, we couldn't
really believe it at first, it sounded like too much of a cliche.

To make things worse, we realised our communications system with
Arsinoe had a little built-in preface module that allowed the A.I.
to editorialise about our messages and open a backdoor channel to
Arsinoe. We can easily check any outgoing network traffic from the
outpost, but since the A.I. messages were piggy-backed on ours, we
never noticed it was communicating. All we saw were our outgoing
logs. We are still confused about the fact that the A.I. seemed to
be 'responding' to messages from Arsinoe. We have no record of any
incoming traffic here for three years.

We all agreed to temporally disable the WHYTE A.I. It's been a bit
burdensome. We now have to manually monitor and update the systems
around the outpost. Navy drew up a rota and everyone's been pulling
their weight so far. It's starting to get better. We've united
behind our common frustration with WHYTE and the Arsinoe.

Most of the communication functions of the base were heavily
integrated with the WHYTE, so we're sort of in the dark now. We
have no access to any outside information at all. I finally managed
to rewrite some of the terminal code to allow us to transmit
messages outward using the QEC at least. If Arsinoe is still out
there (doubtful), it may see the messages indirectly. Not that we
can see its responses.

I'm mostly just sending this out now for posterity.

Having said that, I've kept the name 'masking' protocol. I never
understood why it was there in the first place, but I figure it
must have been for a reason. So, just to be safe, I'm keeping it.
I'm still Green, for now.

We should be able to survive here indefinitely. Our garden is still
producing well and there are enough rations to feed four for eight
years.

We've started digging. Navy has some theory about a hidden room
somewhere on the planet. Neither Pink or I really believe him about
it, but there isn't much else to do here. I've studied the mining
equipment thoroughly, and I think I can keep it running for a
while. We should be able to get pretty deep, but it will take some
time with the way the rocks are here.

Pink has pretty much ignored Olive since she was born. It's like
she doesn't really exist for her. It depresses me. I think she will
come around eventually, but it's been hard for Olive, going without
attention from her mother, and hard for me too.

I hope we find something down there.

===
Sent via the QEC

