
    title=$(curl -sL "$1" | grep -om1 '<title>.*<\/title>' \
                | sed -e s/'<[\/]*title>'//g \
                      -e s/'&#39;'/"'"/g \
                      -e s/'&#34;'/'"'/g \
                      -e s/'&#8211;'/'-'/g \
                      -e s/'&amp;'/'&'/g \
                      -e s/'&nbsp;'/' '/g)
    [[ "$title" != "" ]] && echo -e "\x1f\00032Title\x1f\03: \x02$title\x02"

