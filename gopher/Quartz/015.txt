A question I sometimes ask myself is where the division is between a hyperwave
and a soul.

Just like with my hyperwave, with my soul I can see off long distances, detect
things that biologically would be undetectable, communicate between worlds.
Perhaps they're somehow linked, unlinkable only by death, much like the mind
dimension and the other abilities human units have. I assume like the body, the
hyperwave won't be able to sustain death -- or at least wouldn't be able to
sustain detachment from the soul.

I'm sure there are plenty of those who don't believe in souls or spirits or any
of the like, but I can't relate to them. I can feel my soul and its
interactions. And I can assure anyone that the pure darkness of space fails to
invoke the absolute terror of the woods at night on Earth. The demons aren't
roaming here -- not yet, anyways -- not where my ship is.

I think that the soul and the hyperwave being linked also would mean that the
hyperwave would be stronger when the soul was as well. When I was a child, I
would view myself very often, sometimes unwillingly, in the third person, and
my memories would be in the third person as well. I don't know how much of this
ability was rooted one way or the other -- hyperwave or soul -- but it makes me
wonder how much my hyperwave traveled before I was truly conscious enough to
remember it.

Sometimes I wonder if there are entire worlds out there that I've forgotten. Or
entire selves.

X29
