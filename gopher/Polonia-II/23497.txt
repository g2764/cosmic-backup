# Dwadzieścia trzy tysiące czterysta dziewięćdziesiąty pierwszy dzień lotu

Na mostku burzliwa rozmowa.  
— Kapitanie, to nie my!  
— A kto?!

Jeden z oficerów na skutek zespołu chorobowego
określanego najczęściej jako ZCPK [1] po raz
kolejny wydał niepoprawne dyspozycje
automatycznemu telegrafiście.  Najlepsi
automatyczni telegrafiści byli produkowani jeszcze
na Ziemi w okolicach Zgierza, ale ze względów
ekonomicznych Polonia II wyposażono w sprzęt marki
HPD importowany z USA. Sprzęt był tańszy, ale
często zawodził i do tego wymagał obsługi
głosowej, do tego w dosyć niszowym języku
angielskim. 

Kapitan przeczuwał, że oficer odpowiedzialny tego
dnia za automatycznego telegrafistę pomylił
komendę głosową EXIST z EXITS. Było to nagminne w
trakcie długiego lotu, kiedy trzeba było operować
za pomocą komend głosowych, a nie telepatii. W
kosmosie podczas tak długich podróży rzadko
prowadziło się rozmowy głosowe, a tym bardziej po
angielsku. 

Już po polsku Kapitan zanotował w dzienniku
pokładowym informację adresowaną do CSK w Pułtusku
— Incydent nr 346 sterowania głosowego w języku
angielskim automatycznego telegrafisty marki HPD.
Wniosek o wycofanie z użycia. 

Nie wiedział czy Pułtusk i Zgierz wciąż
funkcjonują po 23491 dniach od czasu, kiedy
ostatni raz komunikacja była dwukierunkowa. Brak
komunikacji zwrotnej nie martwił go tak jak
incydent 346. Napęd Polonia II pozwalał już po 3
godzinach od startu na wyjście z obszaru
komunikacyjnego SOL. 

[1] Zespół Chronicznego Przemęczenia Kosmicznego 
[2] Centrala Sił Kosmicznych
