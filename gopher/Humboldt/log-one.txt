        FROM: Cdr. Rida Sullian
        TO: ATP Central
        SUBJECT: Arrival

        --BEGIN MESSAGE--

    STATUS UPDATE:
    The crew were awakened from cryosleep on schedule. 16/18 survived. We
lost Valton and Parsons. The crew is in mourning, but otherwise,
morale is good. Mostly, we're glad as many of us survived as we did.
All systems nominal. We had a malfunction with the recycler, but it's
been fixed. Our water is going to taste extra salty for a few days, though.
We are within 100,000,000,000km of our destination. Unexpected orbital
structure noted: possibly artificial? Unusual readings, must look into
it.

    The system definitely shows signs of a spacefaring civilization! We've
been attempting to make contact, but we're having difficulty parsing
signals. I expect the same is true for them. Still, I'm excited. We
knew there was a good chance we weren't alone when we first got those 
signals all those years ago, and now we've confirmed it! Let's just
hope they're friendly.

    We'll give you a shout when something happens.

        --END MESSAGE--
