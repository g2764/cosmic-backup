Went fishing today and I actually caught something! It wasn't much
of a something so we put it back in the lake to get bigger. I was
the only one that caught anything, though, so I win. Emmi forever!

The caves below grandpa's den stretch down a clip or two but the
lake isn't so far and there's juice the whole way since this town
is mostly classy bodies that finished their service already. When
you age out of service it's pretty common here to find a den with
loads of hobbies and friends nearby, like grandpa did. I love the
parkways with proper lights too. The greenery is so lush you can
imagine you're walking surface-side through a garden. The air
feels clean and crispy too. Grandpa claims he can walk all the way
to the lake with his eyes closed by the smell of the greens.

When we skidded down to lake-level there were no other bodies. The
parkway there opens into a great dome over the water that the
'geers left raw with stalactites. They drip-drip very slowly into
the pool below with hollow echoes that you can't proper hear
unless everyone stays very still and quiet. But that's sort of
what fishing is, earnest truth. Mum talks a bit too much but but
then grandpa gives her a look. Her words dry up and the peace is
back upon us. I close my eyes and the drips are like church bells.

My head goes crazy with playdreams in the deep quiet. I don't know
what to write about that.

We made it a picnic, which is a wild word to say. Pick-Nick.
I suss it means you bring food with you where you're going, which
doesn't seem like it needs a special word at all. I bring my food
to school every day, but that's not a picnic. It's a classy word,
says mum, which at least makes sense. None of the classy old world
guz makes much sense if you ask me. Grandpa did say I should try
to use more of that in these wires to you. 'E says you might not
know about our proper talks here. I thought he was making fancy
again with the truth at first but some of the things I've read on
the QEC sound proper strange.

Anywise, Uusi Jyväskylä has been a great holiday. Grandpa's razzy
and so is his quirky little town. Even so, I'll be joyed to see my
friends again soon. I miss Ezri and Johanna fierce as damn.
