----------------------------------------
Preparations log 05
--
Subject: SOL
Stardate: 698632.84
----------------------------------------

In preparation for the upcoming milestone, we travelled to several Commerce Hubs
to acquire missing provisions. We needn't go far for this.

List of purchase:
- Food
- Light and small garments, size 56
- Recharging station covers
- Pod light covers
- Metal tubes
- Manual wind generators

We were notified to take the pod to an examination center tomorrow, we're unsure
why. It does not sound like something we need. But alas.

Levels are OK.

