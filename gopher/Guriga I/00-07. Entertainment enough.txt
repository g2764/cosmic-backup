----------------------------------------
Preparations log 07
--
Subject: SOL
Stardate: 698645.49
----------------------------------------

Entertainment day was a blast!
Everyone liked that.

It seems the subject has also enjoyed it, given the movements inside. We will
not submit the subject to more adrenalin in the coming times.

The pod has began to stiffen at times, nothing out of the ordinary. We kept
submerging the pod in water from time to time to cool down even if the storms
have gone. We are having rain today, which is a blessing.

