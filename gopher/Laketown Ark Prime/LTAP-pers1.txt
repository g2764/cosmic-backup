From: vp0a37-σ <vp0a37-σ@comterm1.ltap.vhep>
To: vp0e44-κ <vp0e44-κ@trap8.ltap.vhep>
To: Laketown Command <ltc@ltc.vhep>
To: VHEP Council <councillors@council.vhep>
Delivered-To: Laketown Command <ltc@ltc.vhep>
Received: from relay4.qec1.rs001.l4.earthsys.gov
    by mta2.ltc.vhep
    with ESMTPS id d6kerl43al9g8jl0g
    for <ltc@ltc.vhep>
Received: from relay6.qec7.ganymede.earthsys.gov
    by relay4.qec1.rs001.l4.earthsys.gov
Received: from qec4.helio.earthsys.gov
    by relay3.qec2.ganymede.earthsys.gov
Received: from qec.trap8.ltc.vhep
    by qec4.helio.earthsys.gov
Received: from qec.sltap66
    by qec.trap8.ltc.vhep
Delivered-To: vp0e44-κ <vp0e44-κ@trap8.ltap.vhep>
Received: from qec.sltap66
    by qec.trap8.ltc.vhep
Date-Local: 28 Mar 2436 11:15:03 GSRT +0015y
Date: 18 Dec 2463 17:27:44 -0042y
Subject: Personal Communication
Content-Type: text/plain; charset="utf8"

vp0e44-κ,

LTAP crew consensus allows me to say this:
You are not alone.

We have begun to receive the QEC Archive feed.
Thank you.

Give my regards to the crew.

END MESSAGE.
