Transmission 1.
NGC9550 system. All functions operating normal.
--
First transmission after succesful launch of our Sanchong probe in the
NGC9550 System. Test message packet was transmitted via the short wave
link through the central mass of the pulsar at the heart of the
cluster.
