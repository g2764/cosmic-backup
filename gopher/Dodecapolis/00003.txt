USS Kraken, Captain's Log 00001
===============================

Captain Enzo Avery
USS Kraken

The dodecads received their final assignments today.

A bit of a surprise if I'm being honest: the Kraken is to join Twilight Dusk
under Ganesh Singh of the Jerakeen.

I must say I don't care much for Fleet Commander Rivers's brand of leadership.
Her loyalties are unpredictable. She shows no sense of allegiance, nor does she
hold to our customs and traditions.

For many of the same reasons, nor am I especially crazy about her right hand
man, Wash.

Nevertheless, I'm not above saying that Singh was most impressive during the
evaluations, especially considering his relative inexperience.

And so I readily accept my role and will ensure that Kraken and Shardik serve
Jerakeen well, and that if SRU Dodecapolis fails because of Rivers's chaotic
whims, it will not be because of Twilight Dusk.

Furthermore, experience has shown me time and again that I need not necessarily
be in the seat of power in order to exert my will. With me at his side, I'm
sure that Singh will be an exemplary squad leader, and that Twilight Dusk will
be the pride of Dodecapolis.

EOF
