      ·   ·   ·
       :  :  :               .              
  ~     :·:·:            ···:::··         
 ~~~ D============================D--
  ~     :.:.:             ··:::··

     ####    ##  ## ######  #####
   ##    #   ## ##  ##      ##   #      
   ####### * ##   * ####  * #####
   ##    #   ## ##  ##      # ## 
   ##    #   ##  ## #####   #   ##
  ==================================
   --- Long Range Scout Vessel ---
  ==================================

   Stardate KL.1K
   Stardate protocol Enteka XHEX
   Location .43 ly to Alpha Karenei 3
  -----------------------------------

We have received what appears to be a distress call from a spacecraft
we have been unable to identify. The call seems to be automated and
multilingual, yet only 2 of the languages or codes used could be identified.
Our linguistical department is working ferociously on it.

As per Enteka's command standard procedure we are in REDCON-2. EDFAM
command isalerted, placing usual protections for children and elderly. Two
fighter patrols are on station, together with an EW craft. The ship's
pinnace has been sent to investigate the distress call, together with a
four fighters escort.

~ Capt. R. Firefly

Compressed data line follows: 
ADSFA::SXAESNWT1E#1K2A050kfw342HSDFADF321U-ZA3E0;, 

  
