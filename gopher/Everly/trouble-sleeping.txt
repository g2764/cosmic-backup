###BEGIN TRANSMISSION###
Ship: Everly
From: Alexander
Subject: Trouble Sleeping

Lately I've begun to hear what I can only describe as metal clangs
throughout my night cycle. I've checked with the sensors team a few
times already this week, but they haven't detected anything, at least
externally, that would be causing a disturbance. No one seems too
terribly concerned with it just yet, but it seems to be getting
increasingly more common as the weeks drone on.

###END TRANSMISSION###