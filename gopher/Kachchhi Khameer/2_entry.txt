+----------------------------------------------------------------+
| Project Code : MOKE KHABAR                                     |
| Entry        : 0002                                            |
| Subject      : Installment                                     |
| Author       : માડુ ૪૪૫૫૧૨                                      |
+----------------------------------------------------------------+

Following is the entry I found, from the ships global journal.
------------------------------------------------------------------

Out of cryo! Again?

As memories start coming back, I find myself smiling at more and more
people.

Everything was normal as I came out - bleeping terminals, steady
lights, no smell, two androids standing by.

The doc told me I will get to read a few things this time.

After food, I ran to my room to access the library. I found an old
book which I did not look at. I jacked into a "different" network and
found an awesome poem by "કલાપી":


                રે રે! કિંતુ ફરી કદી પાસ મ્હારી ન આવે,
                આવે તોયે ડરી ડરી અને ઈચ્છતું ઉડવાને;
                રે રે! શ્રદ્ધા ગત થઇ પછી કોઇ કાળે ન આવે,
                લાગ્યા ઘાને વીસરી શકવા કાંઇ સામર્થ્ય ના છે.

Anyway, I will try and dig more into the stuff I have been trying to,
as memories come back.

Till then, અચિજા!

------------------------------------------------------------------

This has raised more questions than it has answered. Following are the
questions I have:

1. What is that poem?
2. Who was "કલાપી"?
3. What does the poem even mean?
4. What have they been trying to "dig"?
5. What memories? Where did they go?
6. What was going in/out of cryo for?
7. Where was this library?

I shall keep my focus on finding more entries from this journal they
had. I believe it is going to be the key to everything.
