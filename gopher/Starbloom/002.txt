Message Incoming...

Source Starbloom Communal Distributive Projection
Approach γ Aquarii [Lucky Homes]
Ascension 22h 21m 39.37542s
Declination –01° 23′ 14.4031″
Distance 178.211ly
Equinox J2000.0 SOL
Year 2444, QEC adjusted

[Autotranslator enabled...]

Susan, Speaker Elect of Cheer Systems, 14th Pod
:::
Merry Meet, Malkonkordo~

Our community is sending you our very best wishes and prayers in
light of your troubling situation. By the sound of things, you've
been away from SOL for quite a long time and your records about
the QEC may be incomplete.

May the everpresent blessings of Diana guard your souls from
despair as we share the news: the QEC is a quantum communicator
and may not be close to your current position in three-dimensional
space after all. Our engineering cats suggest that you may have
fallen back upon an ancient system that was lost to your records.
If that is the case and you don't have detailed information about
SOL, you may be very far away indeed.

The spirits of humanity have gone with you, though, and we here at
the Lucky Homes share in those same spirits now. We will offer
whatever aid we may, though it be moral and spiritual if not
physical. The circle is never broken and your sprits will forever
entwine with all other people's. Like flowers growing in the
spring you will see sun again and flourish. Of this we are
certain. All of our auguries have shown success, though only at
the culmination of many calamities and trials. Keep faith and hope
and love close around you. We shall send more from our abundant
stores.
.
